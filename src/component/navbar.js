import React, { Component } from "react";
import "../css/navbar.css";
import img from "../img/1.png";
import user from "../img/user.png";
import imgB from "../img/back.png";
import icon1 from "../img/icon1.png";
import icon2 from "../img/icon2.png";
import icon3 from "../img/icon3.png";
import icon4 from "../img/icon4.png";
import icon5 from "../img/icon5.png";
import icon6 from "../img/icon6.png";
import icon7 from "../img/icon7.png";
import icon8 from "../img/icon8.png";
import icon9 from "../img/icon9.png";
import icon10 from "../img/icon10.png";
import down from "../img/arrow down.png";
import edit from "../img/edit.png";
import logout from "../img/logout.png";
import line from "../img/line.png";
import { withRouter } from "react-router-dom";
import { post, get } from "../service/api";

class navbar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: ""
    };
  }
  componentDidMount = async () => {
    let res = localStorage.getItem("empID");
    let empID = res;
    try {
      let res = await post("/profile", { empID });
      if (res.success === true) {
        this.setState({
          username: res.result[0].userName
        });
        // this.props.set_user(res.result[0]); //เช็ตข้อมูลลง redux
      }
    } catch (error) {
      alert(error);
    }
  };
  logouts = async () => {
    await get("/logout");
    localStorage.clear();
    this.props.history.push("/");
  };
  render() {
    // console.log("Render", this.state.username);
    return (
      <div className="ca1" style={{ backgroundImage: `url(${imgB})` }}>
        <div className="ca5">
          <div className="ca7">
            <a
              href="/Screen_home"
              title="officeone"
              class="logo-officeone"
              data-metrics="home"
              className="ca4"
            >
              <img src={img} style={{ width: "100px" }} />
            </a>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <div style={{ paddingRight: "5px" }}>
                <img style={{ height: "32px" }} src={user} />
              </div>
              <div id="click-dropdown">
                <div id="div-text">{this.state.username}</div>
                <div>
                  <div id="btn-cnblock">
                    <img src={down} alt="err" id="cnblock" />
                  </div>
                  <div id="show-dropdown">
                    <div id="style-show-dropdown">
                      <div id="syle-show-dropdown-div">
                        <img
                          src={edit}
                          style={{
                            width: "12px",
                            paddingTop: "2px",
                            paddingRight: "3px"
                          }}
                        />
                        <div>เปลี่ยนรูปโปรไฟล์</div>
                      </div>
                    </div>
                    <div
                      style={{
                        width: "100%",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <img src={line} style={{ width: "90%", height: "7px" }} />
                    </div>
                    <div
                      id="style-show-dropdown"
                      onClick={this.logouts.bind(this)}
                    >
                      <div id="syle-show-dropdown-div">
                        <img
                          src={logout}
                          style={{
                            width: "12px",
                            paddingTop: "2px",
                            paddingRight: "3px"
                          }}
                        />
                        <div>ออกจากระบบ</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="ca6">
          <div className="ca9">
            <div style={{ width: "150px", display: "inline-block" }} />
            <div class="dd">
              <span>Leave</span>
              <div class="dd-content">
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    width: "100%"
                  }}
                >
                  <div
                    style={{
                      borderRight: "1px solid",
                      borderColor: "rgba(255, 255, 255, 0.3)",
                      margin: "20px 0",
                      width: "70%"
                    }}
                  >
                    <div
                      style={{
                        overflow: "hidden",
                        marginTop: "30px",
                        marginBottom: "20px",
                        display: "flex",
                        fontSize: "9px",
                        justifyContent: "space-evenly"
                      }}
                    >
                      <a href="/Screen_leaverequest" className="animetion">
                        <div style={{ overflow: "hidden", height: "50px" }}>
                          <img src={icon1} style={{ height: "100%" }} />
                        </div>
                        <div
                          style={{
                            flexDirection: "column",
                            display: "flex",
                            alignItems: "center"
                          }}
                        >
                          <text style={{ position: "absolute" }}>leave</text>
                          <text style={{ marginTop: "14px" }}>request</text>
                        </div>
                      </a>
                      <a href="/Screen_flexible" className="animetion2">
                        <div style={{ overflow: "hidden", height: "50px" }}>
                          <img src={icon2} style={{ height: "100%" }} />
                        </div>
                        <div
                          style={{
                            flexDirection: "column",
                            display: "flex",
                            alignItems: "center"
                          }}
                        >
                          <text style={{ position: "absolute" }}>flexible</text>
                          <text style={{ marginTop: "14px" }}>benefit</text>
                        </div>
                      </a>
                      <a href="/Screen_user" className="animetion3">
                        <div style={{ overflow: "hidden", height: "50px" }}>
                          <img src={icon3} style={{ height: "100%" }} />
                        </div>
                        <div>user</div>
                      </a>
                      <a href="/Screen_company" className="animetion4">
                        <div style={{ overflow: "hidden", height: "50px" }}>
                          <img src={icon4} style={{ height: "100%" }} />
                        </div>
                        <div>company</div>
                      </a>
                      <a href="/Screen_myleaverequest" className="animetion5">
                        <div style={{ overflow: "hidden", height: "50px" }}>
                          <img src={icon5} style={{ height: "100%" }} />
                        </div>
                        <div
                          style={{
                            flexDirection: "column",
                            display: "flex",
                            alignItems: "center"
                          }}
                        >
                          <text style={{ position: "absolute" }}>my leave</text>
                          <text style={{ marginTop: "14px" }}>request</text>
                        </div>
                      </a>
                      <a
                        href="/Screen_myflexiblerequest"
                        className="animetion5"
                      >
                        <div style={{ overflow: "hidden", height: "50px" }}>
                          <img src={icon8} style={{ height: "100%" }} />
                        </div>
                        <div
                          style={{
                            flexDirection: "column",
                            display: "flex",
                            alignItems: "center"
                          }}
                        >
                          <text style={{ position: "absolute" }}>
                            my flexible
                          </text>
                          <text style={{ marginTop: "14px" }}>request</text>
                        </div>
                      </a>
                    </div>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <div
                        style={{
                          borderTop: "solid 1px rgba(255, 255, 255, 0.3)",
                          width: "125px",
                          color: "rgba(255, 255, 255, 0.3)"
                        }}
                      >
                        office one
                      </div>
                    </div>
                  </div>
                  <div style={{ width: "30%" }}>
                    <div
                      style={{
                        overflow: "hidden",
                        marginTop: "30px",
                        marginBottom: "20px",
                        display: "flex",
                        fontSize: "9px",
                        justifyContent: "space-evenly"
                      }}
                    >
                      <a href="/Screen_report_leave" className="animetion6">
                        <div style={{ overflow: "hidden", height: "50px" }}>
                          <img src={icon6} style={{ height: "100%" }} />
                        </div>
                        <div
                          style={{
                            flexDirection: "column",
                            display: "flex",
                            alignItems: "center"
                          }}
                        >
                          <text style={{ position: "absolute" }}>report</text>
                          <text style={{ marginTop: "14px" }}>leave</text>
                        </div>
                      </a>
                      <a href="/Screen_report_flexible" className="animetion7">
                        <div style={{ overflow: "hidden", height: "50px" }}>
                          <img src={icon7} style={{ height: "100%" }} />
                        </div>
                        <div
                          style={{
                            flexDirection: "column",
                            display: "flex",
                            alignItems: "center"
                          }}
                        >
                          <text style={{ position: "absolute" }}>report</text>
                          <text style={{ marginTop: "14px" }}>flexible</text>
                        </div>
                      </a>
                    </div>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <div
                        style={{
                          borderTop: "solid 1px rgba(255, 255, 255, 0.3)",
                          width: "100px",
                          color: "rgba(255, 255, 255, 0.3)"
                        }}
                      >
                        report
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="dd">
              <span>Working time</span>
              <div class="dd-content-1">
                <div
                  style={{
                    overflow: "hidden",
                    marginTop: "20px"
                  }}
                >
                  <div
                    style={{
                      overflow: "hidden",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      margin: "20px 0",
                      fontSize: "9px"
                    }}
                  >
                    <div style={{ marginRight: "50px" }}>
                      <a href="/Screen_work" className="animetion-1">
                        <div style={{ overflow: "hidden", height: "50px" }}>
                          <img src={icon9} style={{ height: "100%" }} />
                        </div>
                        <div>work</div>
                      </a>
                    </div>
                    <div>
                      <a href="/Screen_project" className="animetion-2">
                        <div style={{ overflow: "hidden", height: "50px" }}>
                          <img src={icon10} style={{ height: "100%" }} />
                        </div>
                        <div>project</div>
                      </a>
                    </div>
                  </div>
                </div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <div
                    style={{
                      borderTop: "solid 1px rgba(255, 255, 255, 0.3)",
                      width: "100px",
                      color: "rgba(255, 255, 255, 0.3)"
                    }}
                  >
                    office one
                  </div>
                </div>
              </div>
            </div>
            <a href="/Screen_quest" id="div-quest">
              <a href="/Screen_quest" id="div-quest-a">
                quest
              </a>
            </a>
          </div>
        </div>
      </div>
    );
  }
}
export default withRouter(navbar);
