export const sever = "http://localhost:3108/api/v1/auth/";

export const post = (path, object, token) => {
  return new Promise((resolve, reject) => {
    fetch(sever + path, {
      method: "post",
      headers: {
        Authorization: token,
        "Content-Type": "application/json"
      },
      body: JSON.stringify(object),
      credentials: "include"
    })
      .then(Response => Response.json())
      .then(json => resolve(json))
      .catch(err => reject(err));
  });
};

export const postExport = (path, object, token) => {
  return new Promise((resolve, reject) => {
    fetch(sever + path, {
      method: "get",
      headers: {
        Authorization: token,
        "Content-Type": "application/json"
      },
      params: object,
      credentials: "include"
    })
      .then(Response => resolve(Response))
      .catch(err => reject(err));
  });
};

export const get = (path, token) => {
  return new Promise((resolve, reject) => {
    fetch(sever + path, {
      method: "get",
      headers: {
        Authorization: token,
        "Content-Type": "application/json"
      },
      credentials: "include"
    })
      .then(Response => Response.json())
      .then(json => resolve(json))
      .catch(err => reject(err));
  });
};
