import React, { Component } from "react";
import "./App.css";
import Navbar from "./component/navbar";

class App extends Component {
  render() {
    const { children } = this.props;
    return (
      <div style={{ display: "block" }}>
        <Navbar />
        {children}
      </div>
    );
  }
}

export default App;
