import { SET_USER, REMOVE_USER } from "./user_type";

export const set_user = payload => ({ type: SET_USER, payload });
export const remove_user = payload => ({ type: REMOVE_USER, payload });
