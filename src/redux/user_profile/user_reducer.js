import { SET_USER } from "./user_type";
import { REMOVE_USER } from "./user_type";

export default (state = {}, action) => {
  switch (action.type) {
    case SET_USER:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};
