import { combineReducers } from "redux";
import userreducer from "./user_profile/user_reducer";

export default combineReducers({
  user: userreducer
});
