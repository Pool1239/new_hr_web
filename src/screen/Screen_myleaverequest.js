import React, { Component } from "react";
import "../css/Screen_myleaverequest.css";
import swal from "sweetalert";
import moment from "moment";
import ps from "../img/backhome.png";
import PickyDateTime from "react-picky-date-time";
import { Timepicker } from "react-timepicker";
import "react-timepicker/timepicker.css";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import insert from "../img/insert.png";
import Modal from "react-responsive-modal";
import { post, get } from "../service/api";
var x = [1];
var data = [];
// import { element } from "C:/Users/Administrator/AppData/Local/Microsoft/TypeScript/3.2/node_modules/@types/prop-types";
export default class Screen_myleaverequest extends Component {
  constructor(props) {
    super(props);

    this.state = {
      empID: localStorage.getItem("empID"),
      row: [],
      leaID_type: "",
      leaName_type: "",
      leaName_thai: "",
      leaName_type: "",
      max_day: "",
      id_company: "",
      type: "1",
      showPickyDateTime: true,
      date: "",
      month: "",
      year: "",
      date2: "",
      month2: "",
      year2: "",
      time_start: "",
      time_end: "",
      startDate: new Date(),
      leave_reason: "",
      dataTables: []
    };
    this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount = async () => {
    try {
      let res = await get("/show_my_leave_request");
      console.log("res", res.result);

      this.setState({ dataTables: res.result });

    } catch (error) {
      alert(error);
    }
  };
  componentWillMount = () => {
    this.getRow();
  };
  state = {
    open: false
  };
  onOpenModal = () => {
    this.setState({ open: true });
  };
  onCloseModal = () => {
    this.setState({ open: false });
    window.location.reload();
  };
  onClick = async element => {
    let leaID_type = element.leaID_type;
    this.setState({
      leaID_type: leaID_type
    });
  };
  onClick2 = e => {
    this.setState({
      type: e.target.value
    });
  };
  onYearPicked(res) {
    const { year } = res;
    this.setState({ year: year });
  }
  onMonthPicked(res) {
    const { month, year } = res;
    this.setState({ year: year, month: month });
  }
  onDatePicked(res) {
    const { date, month, year } = res;
    this.setState({ year: year, month: month, date: date });
  }
  onResetDate(res) {
    const { date, month, year } = res;
    this.setState({ year: year, month: month, date: date });
  }
  onResetDefaultDate(res) {
    const { date, month, year } = res;
    this.setState({ year: year, month: month, date: date });
  }
  onYearPicked2(res) {
    const { year } = res;
    this.setState({ year2: year });
  }
  onMonthPicked2(res) {
    const { month, year } = res;
    this.setState({ year2: year, month2: month });
  }
  onDatePicked2(res) {
    const { date, month, year } = res;
    this.setState({ year2: year, month2: month, date2: date });
  }
  onResetDate2(res) {
    const { date, month, year } = res;
    this.setState({ year2: year, month2: month, date2: date });
  }
  onResetDefaultDate2(res) {
    const { date, month, year } = res;
    this.setState({ year2: year, month2: month, date2: date });
  }
  onChange = async (hours, minutes) => {
    this.setState({
      time_start: hours + ":" + moment(minutes, "mm").format("mm")
    });
  };
  onChange2 = async (hours, minutes) => {
    this.setState({
      time_end: hours + ":" + moment(minutes, "mm").format("mm")
    });
  };
  handleChange(date) {
    this.setState({
      startDate: date
    });
  }
  onChangeTextarea = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onClickTime = async () => {
    if (
      this.state.startDate === "" ||
      this.state.time_start === "" ||
      this.state.time_end === "" ||
      this.state.leave_reason === ""
    ) {
      swal("Warning!", "Please complete the information", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => { });
    } else {
      let date_start =
        moment(this.state.startDate).format("YYYY-MM-DD") +
        " " +
        this.state.time_start;
      let date_end =
        moment(this.state.startDate).format("YYYY-MM-DD") +
        " " +
        this.state.time_end;
      let obj = {
        empID:this.state.empID,
        leaID_type: this.state.leaID_type,
        leaID_day: this.state.type,
        date_start,
        date_end,
        lea_reason: this.state.leave_reason
      };
      console.log(JSON.stringify(obj));
      try {
        let res = await post("/insert_my_leave_request",obj);
        swal("Complete", " ", "success", {
          buttons: false,
          timer: 1200
        }).then(() => {
          window.location.reload();
        });
      } catch (error) {
        swal("Warning!", " ", "warning", {
          buttons: false,
          timer: 1200
        }).then(() => {});
      }
    }
  };
  onClickDate = async () => {
    if (
      this.state.year === "" ||
      this.state.year2 === "" ||
      this.state.month === "" ||
      this.state.month2 === "" ||
      this.state.date === "" ||
      this.state.date2 === "" ||
      this.state.leave_reason === ""
    ) {
      swal("Warning!", "Please complete the information", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => { });
    } else {
      let date_start =
        this.state.year + "-" + this.state.month + "-" + this.state.date;
      let date_end =
        this.state.year2 + "-" + this.state.month2 + "-" + this.state.date2;
      let obj = {
        empID:this.state.empID,
        leaID_type: this.state.leaID_type,
        leaID_day: this.state.type,
        date_start,
        date_end,
        lea_reason: this.state.leave_reason
      };
      console.log(JSON.stringify(obj));
      try {
        let res = await post("/insert_my_leave_request",obj);
        swal("Complete", " ", "success", {
          buttons: false,
          timer: 1200
        }).then(() => {
          window.location.reload();
        });
      } catch (error) {
        swal("Warning!", " ", "warning", {
          buttons: false,
          timer: 1200
        }).then(() => {});
      }
    }
  };
  onChangeCreate = (e, i) => {
    data[i] = {
      ...data[i],
      [e.target.name]: e.target.value,
      IDcompany: "1"
    };
    console.log("Create", data);
  };
  onClickInsertCreate = async () => {
    x.push(1);
    this.getRow();
  };
  onClickCreate = async i => {
    const a = data;
    let leaName_type = [];
    let leaName_thai = [];
    let max_day = [];
    let IDcompany = [];
    for (let i = 0; i < a.length; i++) {
      leaName_type.push(a[i].leaName_type);
      leaName_thai.push(a[i].leaName_thai);
      max_day.push(a[i].max_day);
      IDcompany.push(a[i].IDcompany);
    }
    const obj = {
      leaName_type,
      leaName_thai,
      max_day,
      IDcompany,
      empID: this.state.empID
    };
    console.log("obj", obj);
    try {
      await post('/create_my_leave_request', obj)
      swal("Complete", " ", "success", {
        buttons: false,
        timer: 1200
      }).then(() => {
        this.onCloseModal();
        // window.location.reload();
      });
    } catch (error) {
      swal("Warning!", " ", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => {});
    }
  };
  getRow() {
    let row = x.map((ele, i) => {
      return this.renderRow(i);
    });
    this.setState({ row });
  }
  renderRow(i) {
    return (
      <div style={{ display: "flex" }}>
        <text style={{ marginRight: 5, color: "lightcyan" }}>{i + 1}.</text>
        <input
          type="text"
          name="leaName_type"
          className="cmrbt1"
          onChange={e => this.onChangeCreate(e, i)}
        />
        <div style={{ color: "lightcyan" }}>--</div>
        <input
          type="text"
          name="leaName_thai"
          className="cmrbt1"
          onChange={e => this.onChangeCreate(e, i)}
        />
        <div style={{ color: "lightcyan" }}>--</div>
        <input
          type="number"
          name="max_day"
          className="cmrbt2"
          onChange={e => this.onChangeCreate(e, i)}
        />
      </div>
    );
  }
  render() {
    const { open, row } = this.state;
    return (
      <div className="cmr1">
        <div className="cmr2">
          <div className="cmr3">
            <div className="cmr3-1">my leave request</div>
            <div>
              <button
                type="button"
                className="cmr3-2"
                onClick={this.onOpenModal}
              >
                <img
                  src={insert}
                  style={{ width: "15px", marginRight: "2px" }}
                />
                <div>CREATE</div>
              </button>
            </div>
          </div>
          <div className="cmr4">
            <div className="cmr5">
              {this.state.dataTables.map(element => {
                return (
                  <div className="cmr8">
                    <button
                      className="cmbt"
                      onClick={() => this.onClick(element)}
                    >
                      <div>
                        <div style={{ marginBottom: "5px" }}>
                          {element.leaName_type}
                        </div>
                        <div>{element.leaName_thai}</div>
                      </div>
                    </button>
                  </div>
                );
              })}
            </div>
            <div className="cmr6">
              {this.state.leaID_type === "" ? (
                <img src={ps} style={{ width: "140%" }} />
              ) : this.state.type === "1" ? (
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-evenly",
                    width: "100%"
                  }}
                >
                  <div className="cpdt">
                    <PickyDateTime
                      size="s"
                      mode={0}
                      locale="en-us"
                      show={true}
                      onYearPicked={res => this.onYearPicked(res)}
                      onMonthPicked={res => this.onMonthPicked(res)}
                      onDatePicked={res => this.onDatePicked(res)}
                      onResetDate={res => this.onResetDate(res)}
                      onResetDefaultDate={res => this.onResetDefaultDate(res)}
                    />
                  </div>
                  <div className="cpdt2">
                    <PickyDateTime
                      size="s"
                      mode={0}
                      locale="en-us"
                      show={true}
                      onYearPicked={res => this.onYearPicked2(res)}
                      onMonthPicked={res => this.onMonthPicked2(res)}
                      onDatePicked={res => this.onDatePicked2(res)}
                      onResetDate={res => this.onResetDate2(res)}
                      onResetDefaultDate={res => this.onResetDefaultDate2(res)}
                    />
                  </div>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                      width: "25%",
                      height: "400px",
                      flexDirection: "column"
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        width: "100%",
                        height: "35%"
                      }}
                    >
                      <button
                        value={1}
                        name="type"
                        className="cmbt3"
                        onClick={this.onClick2}
                      >
                        full day
                      </button>
                      <button
                        value={2}
                        name="type"
                        className="cmbt4"
                        onClick={this.onClick2}
                      >
                        haft day
                      </button>
                    </div>
                    <div
                      style={{
                        width: "100%",
                        height: "52%",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        flexDirection: "column"
                      }}
                    >
                      <div style={{ color: "lightcyan", marginBottom: "2px" }}>
                        Please Enter The Message
                      </div>
                      <textarea
                        className="cpla"
                        style={{
                          textAlign: "center",
                          width: "150px",
                          height: "50%",
                          color: "lightcyan",
                          border: "none",
                          backgroundColor: "transparent",
                          boxShadow:
                            "rgba(0, 0, 0, 0.1) 1px 1px 4px 0px, rgba(0, 0, 0, 0.08) 0px 0px 4px 0px"
                        }}
                        name="leave_reason"
                        placeholder="message"
                        onChange={this.onChangeTextarea}
                      />
                    </div>
                    <div
                      style={{
                        width: "100%",
                        height: "12%",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <input
                        id="confirm-btn"
                        name="submit"
                        type="submit"
                        value="submit"
                        className="cbtns"
                        onClick={this.onClickDate}
                      />
                    </div>
                  </div>
                </div>
              ) : (
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "space-evenly",
                        width: "100%",
                        height: "100%"
                      }}
                    >
                      <div
                        style={{
                          height: "398px",
                          boxShadow:
                            "1px 1px 4px 0 rgba(0,0,0,.1), 0 0 4px 0 rgba(0,0,0,.08)",
                          width: "322px",
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                        className="cpdt3"
                      >
                        <Timepicker
                          timeMode="24"
                          timezone="Asia/Bangkok"
                          onChange={this.onChange}
                        />
                      </div>
                      <div
                        style={{
                          height: "398px",
                          boxShadow:
                            "1px 1px 4px 0 rgba(0,0,0,.1), 0 0 4px 0 rgba(0,0,0,.08)",
                          width: "322px",
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                        className="cpdt4"
                      >
                        <Timepicker
                          timeMode="24"
                          timezone="Asia/Bangkok"
                          onChange={this.onChange2}
                        />
                      </div>
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          width: "25%",
                          height: "100%",
                          flexDirection: "column"
                        }}
                      >
                        <div
                          style={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            width: "100%",
                            height: "35%"
                          }}
                        >
                          <button
                            value={1}
                            name="type"
                            className="cmbt3"
                            onClick={this.onClick2}
                          >
                            full day
                      </button>
                          <button
                            value={2}
                            name="type"
                            className="cmbt4"
                            onClick={this.onClick2}
                          >
                            haft day
                      </button>
                        </div>
                        <div
                          style={{
                            width: "100%",
                            height: "20%",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            flexDirection: "column"
                          }}
                        >
                          <div style={{ color: "lightcyan", marginBottom: "2px" }}>
                            Please Select Date
                      </div>
                          <DatePicker
                            className="cinput"
                            selected={this.state.startDate}
                            onChange={this.handleChange}
                          />
                        </div>
                        <div
                          style={{
                            width: "100%",
                            height: "32%",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            flexDirection: "column"
                          }}
                        >
                          <div style={{ color: "lightcyan", marginBottom: "2px" }}>
                            Please Enter The Message
                      </div>
                          <textarea
                            className="cpla"
                            style={{
                              textAlign: "center",
                              width: "150px",
                              height: "50%",
                              color: "lightcyan",
                              border: "none",
                              backgroundColor: "transparent",
                              boxShadow:
                                "rgba(0, 0, 0, 0.1) 1px 1px 4px 0px, rgba(0, 0, 0, 0.08) 0px 0px 4px 0px"
                            }}
                            name="leave_reason"
                            placeholder="message"
                            onChange={this.onChangeTextarea}
                          />
                        </div>
                        <div
                          style={{
                            width: "100%",
                            height: "12%",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center"
                          }}
                        >
                          <input
                            id="confirm-btn"
                            name="sunmit"
                            type="button"
                            value="submit"
                            className="cbtns"
                            onClick={this.onClickTime}
                          />
                        </div>
                      </div>
                    </div>
                  )}
              <div />
            </div>
          </div>
        </div>
        <Modal open={open} onClose={this.onCloseModal}>
          <div>
            <div className="cmdc">CREATE TYPE LEAVE</div>
          </div>
          <div
            style={{
              width: "100%",
              display: "flex",
              justifyContent: "space-around",
              alignItems: "center",
              flexDirection: "row",
              color: "lightcyan"
            }}
          >
            <div style={{ width: "4%" }} />
            <div style={{ width: "40%" }}>ชื่ออังกฤษ</div>
            <div style={{ width: "40%" }}>ชื่อไทย</div>
            <div style={{ width: "20%" }}>จำนวนวัน</div>
          </div>
          {row}
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                width: "96%"
              }}
            >
              <button onClick={this.onClickInsertCreate} className="cmdc3">
                เพิ่มช่อง
              </button>
              <button onClick={this.onClickCreate} className="cmdc3">
                เพิ่มข้อมูล
              </button>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}
