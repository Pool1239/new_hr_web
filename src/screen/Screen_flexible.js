import React, { Component } from "react";
import "../css/Screen_flexible.css";
import "../css/select.css";
import moment from "moment";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import searc from "../img/magnifying-glass.png";
import slip from "../img/slip.jpg"
import Modal from "react-responsive-modal";
import { post, get } from "../service/api";
import swal from "sweetalert"
export default class Screen_flexible extends Component {
  constructor(props) {
    super(props);

    this.state = {
      benID: [],
      full_name: "",
      comname: "",
      benName_type: "",
      ben_date_request: "",
      ben_date_receipt: "",
      amount: "",
      sta_name: "",
      userName: "",
      flexible_created: "",
      email:"",
      dataTables: [],
      img:""
    };
  }

  state = {
    open: false
  };
  componentDidMount = async () => {
    try {
      let res = await get("/alldata_flexible_benefit");
      console.log("res", res.result);

      this.setState({ dataTables: res.result });

    } catch (error) {
      alert(error);
    }
  };
  ButtonApprove = (cell, Row) => {
    return (
      <div style={{ display: "flex" }}>
        <div>
          <img className="button" src={searc} style={{ marginRight: 10, cursor: "pointer" }} onClick={() => this.onOpenModal(Row)} />

        </div>
        <div>
          {Row.sta_name == "Wait" ? (
            <button className="fbt1" onClick={()=>this.onApprove2(Row)}>Approve</button>
          ) : Row.sta_name == "Approve" ? (
            <button disabled className="fbt2">
              Approve
            </button>
          ) : (
                <button disabled className="fbt3">
                  Reject
            </button>
              )}
        </div>

      </div>
    );
  };
  renderSizePerPageDropDown = props => {
    return (
      <div className="btn-group">
        {[10, 25, 30, 40, 50, 100].map((n, idx) => {
          const isActive = n === props.currSizePerPage ? "active" : null;
          return (
            <button
              key={idx}
              type="button"
              className={`btn btn-info ${isActive}`}
              onClick={() => props.changeSizePerPage(n)}
            >
              {n}
            </button>
          );
        })}
      </div>
    );
  };
  onOpenModal = async (Row) => {
    this.setState({
      open: true,
      benID: Row.benID,
      full_name: Row.full_name,
      comname: Row.comname,
      benName_type: Row.benName_type,
      ben_date_request: Row.ben_date_request,
      ben_date_receipt: Row.ben_date_receipt,
      amount: Row.amount,
      sta_name: Row.sta_name,
      userName: Row.userName,
      flexible_created: Row.flexible_created
    });
   

  };
  onCloseModal = () => {
    this.setState({ open: false });
    // window.location.reload();
  };
  onApprove = async () => {

    console.log("benID", this.state.benID);

    let benID = this.state.benID;
    try {
      await post("/approve_statusBen", { benID });

      swal("Complete", "process completed", "success");
      setTimeout(() => {
        window.location.reload();
      }, 1000);

    } catch (error) {
      alert(error);
    }
  }
  onReject = async () => {

    console.log("benID", this.state.benID);

    let benID = this.state.benID;
    try {
      await post("/reject_statusBen", { benID });

      swal("Complete", "process completed", "success");
      setTimeout(() => {
        window.location.reload();
      }, 1000);

    } catch (error) {
      alert(error);
    }
  }
  onApprove2 = async (Row) => {
    
    let benID = Row.benID
    console.log("benID",benID);
    
    try {
      await post("/approve_statusBen", {benID});
      
      swal("Complete", "process completed", "success");
      setTimeout(() => {
        window.location.reload();
      }, 1000);
      
    } catch (error) {
      alert(error);
    }
  }
  render() {
    const { open } = this.state;
    return (
      <div className="cfb1">
        <div className="cfb2">
          <div className="cfb3">flexible request</div>
          <BootstrapTable
            search
            className="ftb"
            data={this.state.dataTables}
            pagination
            bordered={false}
            striped
            options={{
              sizePerPageDropDown: this.renderSizePerPageDropDown
            }}
          >
            <TableHeaderColumn width={80} dataField="benID" dataSort isKey>
              {"ID"}
            </TableHeaderColumn>
            <TableHeaderColumn dataField="full_name" dataSort>
              {"Employee Name"}
            </TableHeaderColumn>
            <TableHeaderColumn dataField="comname" dataSort>
              {"Company Name"}
            </TableHeaderColumn>
            <TableHeaderColumn dataField="benName_type" dataSort>
              {"Benefit Type"}
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="ben_date_request"
              dataSort
              dataFormat={cell => <p>{moment(cell).format("L")}</p>}
            >
              {"Date Request"}
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="ben_date_receipt"
              dataSort
              dataFormat={cell => <p>{moment(cell).format("L")}</p>}
            >
              {"Date Receipt"}
            </TableHeaderColumn>
            <TableHeaderColumn dataField="amount" dataSort>
              {"Amount"}
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="sta_name"
              dataFormat={this._status}
              dataSort
            >
              {"Status"}
            </TableHeaderColumn>
            <TableHeaderColumn dataField="email" dataSort>
              {"Approver"}
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="flexible_created"
              dataSort
              dataFormat={cell => <p>{moment(cell).format("L")}</p>}
            >
              {"Created"}
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="benID"
              dataSort
              dataFormat={this.ButtonApprove}
            >
              {"Management"}
            </TableHeaderColumn>
          </BootstrapTable>
        </div>
        <Modal open={open} onClose={this.onCloseModal} >

          <div className="box">

            <div>
              <div
                style={{
                  textTransform: "uppercase",
                  marginTop: "10px",
                  marginBottom: "15px",
                  color: "lightcyan",
                  fontSize: "large"
                }}
              >
                Manage Request
              </div>
              <div className="custom-select">{this.state.benName_type}</div>
              <div className="custom-dateA">
                <div className="custom-date">{moment(this.state.ben_date_request).format("L")}</div>
                <div className="custom-date">{moment(this.state.ben_date_receipt).format("L")}</div>
              </div>
              <div className="custom-select1">Amount {this.state.amount} Bath</div>
              <div className="custom-buttonA">
                <input
                  type="button"
                  value="APPROVE"
                  className="custom-button"
                  onClick={() => this.onApprove()}
                />
                <input
                  type="button"
                  value="REJECT"
                  className="custom-button1"
                  onClick={() => this.onReject()}
                />
              </div>
            </div>

            <div>
              <div >
                <div>
                  <img src={"http://localhost:3108/api/v1/auth/show_img/"+this.state.benID} className="size-slip" />
                </div>
              </div>
            </div>

          </div>
        </Modal>

      </div>
    );
  }
}
