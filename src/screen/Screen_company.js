import React, { Component } from "react";
import {
  BootstrapTable,
  TableHeaderColumn,
  ButtonGroup
} from "react-bootstrap-table";
import "../css/Screen_company.css";
import { FormControl, FormGroup, ControlLabel } from "react-bootstrap";
import Modal from "react-responsive-modal";
import swal from "sweetalert";
import insert from "../img/insert.png";
import update from "../img/update.png";
import del from "../img/del.png";
import { post, get } from "../service/api";

export default class Screen_company extends Component {
  constructor(props) {
    super(props);

    this.state = {
      comID: [],
      time_in: "",
      time_out: "",
      comname: "",
      members: "",
      dataTables: [],
      dataTables2: []
    };
  }
  componentDidMount = async () => {
    let res = await get("alldata_company");
    this.setState({
      dataTables: res.result.data,
      members: res.result.data2
    });
  };
  state = {
    addCompany: false
  };
  getValidationAddcomnameState() {
    const name = this.state.comname;
    const length = this.state.comname.length;
    if (length >= 1 && name != " ") return "success";
    else if (length >= 1 && name === " ") return "warning";
    else if (length === 0) return "error";
    return null;
  }
  getValidationCompanyTime_InState() {
    const time_in = this.state.time_in;
    const length = this.state.time_in.length;
    if (length >= 1 && time_in != " ") return "success";
    else if (length >= 1 && time_in === " ") return "warning";
    else if (length === 0) return "error";
    return null;
  }
  getValidationCompanyTime_OutState() {
    const time_out = this.state.time_out;
    const length = this.state.time_out.length;
    if (length >= 1 && time_out != " ") return "success";
    else if (length >= 1 && time_out === " ") return "warning";
    else if (length === 0) return "error";
    return null;
  }
  handleAddCompany = e => {
    this.setState({ comname: e.target.value });
  };
  handleAddCompanyTime_In = e => {
    this.setState({ time_in: e.target.value });
  };
  handleAddCompanyTime_Out = e => {
    this.setState({ time_out: e.target.value });
  };
  onCloseAddCompany = () => {
    this.setState({
      addCompany: false,
      comname: "",
      time_in: "",
      time_out: ""
    });
  };

  onChangeUpdate = e => {
    this.setState({ [e.target.dataField]: e.target.value });
  };
  onClickUpdate = async () => {
    try {
      let res = await get("/alldata_company");
      this.setState({ dataTables2: res.result.data });
    } catch (error) {
      alert(error);
    }
    let full_name = this.state.dataTables.map((el, i) => {
      const indexUpdate =
        JSON.stringify(this.state.dataTables2[i]) === JSON.stringify(el); //*** เปรียบเทียบ arry[1] กับ arry[2] เหมือนกันไหม */
      if (!indexUpdate) {
        return { ...el, find: true };
      } else {
        return { ...el, find: false };
      }
    });
    let fine = full_name.map(el => el.find);
    let index = fine.some(el => el == true);
    if (!index) {
      swal("กรุณาทำการแก้ไขข้อมูล", " ", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => {});
    } else {
      let datas = full_name;
      try {
        let res = await post("/update_company", { datas });
        if (res.success === true) {
          swal(res.result, " ", "success", {
            buttons: false,
            timer: 1200
          }).then(() => {
            window.location.reload();
          });
        } else {
          swal(res.message, " ", "warning", {
            buttons: false,
            timer: 1200
          }).then(() => {});
        }
      } catch (error) {
        swal(error, " ", "warning", {
          buttons: false,
          timer: 1200
        }).then(() => {});
      }
    }
  };
  onDeleteRow = async () => {
    if (this.state.comID.length === 0) {
      swal("กรุณาเลือกข้อมูล!", " ", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => {});
    } else {
      swal({
        title: "Are you sure?",
        text: "คุณแน่ใจนะ!",
        icon: "warning",
        buttons: true,
        dangerMode: true
      }).then(async willDelete => {
        if (willDelete) {
          let comID = this.state.comID;
          try {
            let res = await post("/delete_company", { comID });
            swal(res.result, " ", "success", {
              buttons: false,
              timer: 1200
            }).then(() => {
              window.location.reload();
            });
          } catch (error) {
            alert(error);
          }
        }
      });
    }
  };
  onRowSelect = (row, isSelected) => {
    if (isSelected === true) {
      console.log(`selected: ${isSelected}`);
      console.log("row", row.comID);
      // let e = this.state.empID;
      // e.push(row.empID)
      // this.setState({empID: e}) //  [1.2.3] => empID: [1,2,3, row.empID]
      this.setState(prev => ({ comID: [...prev.comID, row.comID] }));
    } else {
      console.log(`selected: ${isSelected}`);
      let e = this.state.comID; // [1,2,3] => [1,2,3].findIndex(el => 1,2,3)
      let index = e.findIndex(el => el === row.comID);
      e.splice(index, 1);
      this.setState({ comID: e });
    }
  };
  onSelectAll = isSelected => {
    console.log(`is select all: ${isSelected}`);
    if (isSelected === true) {
      let e = this.state.dataTables.map(el => el.comID);
      this.setState({ comID: e });
    } else {
      this.setState({ comID: [] });
    }
  };
  onClickInsert = async () => {
    if (
      this.state.comname == "" ||
      this.state.time_in == "" ||
      this.state.time_out == ""
    ) {
      swal("Warning!", "Please fill out this form", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => {});
    } else {
      let comname = this.state.comname;
      let time_in = this.state.time_in;
      let time_out = this.state.time_out;
      let obj = {
        comname,
        time_in,
        time_out
      };
      try {
        let res = await post("/insert_company", obj);
        swal(res.result, " ", "success", {
          buttons: false,
          timer: 1200
        }).then(() => {
          this.onCloseAddCompany();
          window.location.reload();
        });
      } catch (error) {
        swal(error, " ", "warning", {
          buttons: false,
          timer: 1200
        }).then(() => {});
      }
    }
  };
  renderSizePerPageDropDown = props => {
    return (
      <div className="btn-group">
        {[10, 25, 30, 40, 50, 100].map((n, idx) => {
          const isActive = n === props.currSizePerPage ? "active" : null;
          return (
            <button
              key={idx}
              type="button"
              className={`btn btn-info ${isActive}`}
              onClick={() => props.changeSizePerPage(n)}
            >
              {n}
            </button>
          );
        })}
      </div>
    );
  };
  createCustomButtonGroup = () => {
    return (
      <div>
        <ButtonGroup className="my-custom-class" sizeClass="btn-group-md">
          <button
            type="button"
            className="ccbt-insert"
            onClick={() => this.setState({ addCompany: true })}
          >
            <img src={insert} style={{ width: "15px", marginRight: "2px" }} />
            <div>INSERT</div>
          </button>
        </ButtonGroup>
        <ButtonGroup className="my-custom-class" sizeClass="btn-group-md">
          <button
            type="button"
            className="ccbt-update"
            onClick={this.onClickUpdate}
          >
            <img src={update} style={{ width: "15px", marginRight: "2px" }} />
            <div>UPDATE</div>
          </button>
        </ButtonGroup>
        <ButtonGroup className="my-custom-class" sizeClass="btn-group-md">
          <button
            type="button"
            className="ccbt-delete"
            onClick={this.onDeleteRow}
          >
            <img src={del} style={{ width: "15px", marginRight: "2px" }} />
            <div>DELETE</div>
          </button>
        </ButtonGroup>
      </div>
    );
  };
  render() {
    const { addCompany } = this.state;
    const selectRowProp = {
      mode: "checkbox",
      clickToSelect: true,
      onSelect: this.onRowSelect,
      onSelectAll: this.onSelectAll
    };
    return (
      <div className="cc1">
        <div className="cc2">
          <div className="cc3">
            <div style={{ display: "flex" }}>
              <div>company</div>
            </div>
            <div>{this.state.members} company</div>
          </div>
          <BootstrapTable
            data={this.state.dataTables}
            className="ctb"
            pagination
            options={{
              btnGroup: this.createCustomButtonGroup,
              sizePerPageDropDown: this.renderSizePerPageDropDown
            }}
            bordered={false}
            striped
            cellEdit={{
              mode: "click"
            }}
            onChange={this.onChangeUpdate}
            selectRow={selectRowProp}
          >
            <TableHeaderColumn width={80} dataField="comID" dataSort isKey>
              {"ID"}
            </TableHeaderColumn>
            <TableHeaderColumn width={500} dataField="comname" dataSort>
              {"Company"}
            </TableHeaderColumn>
            <TableHeaderColumn width={200} dataField="time_in" dataSort>
              {"Open"}
            </TableHeaderColumn>
            <TableHeaderColumn width={200} dataField="time_out" dataSort>
              {"Close"}
            </TableHeaderColumn>
            <TableHeaderColumn
              width={100}
              dataField="countcompany"
              editable={false}
            >
              {"Members"}
            </TableHeaderColumn>
          </BootstrapTable>
        </div>

        <Modal open={addCompany} onClose={this.onCloseAddCompany} center>
          <div style={{ alignItems: "center", justifyContent: "center" }}>
            <ControlLabel
              style={{
                marginBottom: 15,
                marginTop: 10,
                fontSize: "large",
                color: "lightcyan",
                textTransform: "uppercase",
                fontWeight: "unset"
              }}
            >
              Create Company
            </ControlLabel>
            <FormGroup
              controlId="formBasicText"
              validationState={this.getValidationAddcomnameState()}
            >
              <FormControl
                type="text"
                name="comname"
                value={this.state.comname}
                placeholder="Enter Company Name"
                onChange={this.handleAddCompany}
              />
              <FormControl.Feedback />
            </FormGroup>

            <FormGroup
              controlId="formBasicText"
              validationState={this.getValidationCompanyTime_InState()}
            >
              <FormControl
                type="time"
                name="time_in"
                value={this.state.time_in}
                placeholder="Enter Company Name"
                onChange={this.handleAddCompanyTime_In}
              />
              <FormControl.Feedback />
            </FormGroup>
            <FormGroup
              controlId="formBasicText"
              validationState={this.getValidationCompanyTime_OutState()}
            >
              <FormControl
                type="time"
                name="time_out"
                value={this.state.time_out}
                placeholder="Enter Company Name"
                onChange={this.handleAddCompanyTime_Out}
              />
              <FormControl.Feedback />
            </FormGroup>
            <div flexDirection="row" className="ccm">
              <input
                type="button"
                value="Accept"
                className="cbt"
                onClick={this.onClickInsert}
              />
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}
