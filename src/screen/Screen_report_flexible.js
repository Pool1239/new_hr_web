import React, { Component } from "react";
import PickyDateTime from "react-picky-date-time";
import swal from "sweetalert";
import "../css/Screen_report_flexible.css";
import { post, get } from "../service/api";


export default class Screen_report_flexible extends Component {
  constructor(props) {
    super(props);
    this.state = {
      empID:localStorage.getItem("empID"),
      showPickyDateTime: true,
      date: "",
      month: "",
      year: "",
      date2: "",
      month2: "",
      year2: ""
    };
  }
  onYearPicked(res) {
    const { year } = res;
    this.setState({ year: year });
  }
  onMonthPicked(res) {
    const { month, year } = res;
    this.setState({ year: year, month: month });
  }
  onDatePicked(res) {
    const { date, month, year } = res;
    this.setState({ year: year, month: month, date: date });
  }
  onResetDate(res) {
    const { date, month, year } = res;
    this.setState({ year: year, month: month, date: date });
  }
  onResetDefaultDate(res) {
    const { date, month, year } = res;
    this.setState({ year: year, month: month, date: date });
  }
  onYearPicked2(res) {
    const { year } = res;
    this.setState({ year2: year });
  }
  onMonthPicked2(res) {
    const { month, year } = res;
    this.setState({ year2: year, month2: month });
  }
  onDatePicked2(res) {
    const { date, month, year } = res;
    this.setState({ year2: year, month2: month, date2: date });
  }
  onResetDate2(res) {
    const { date, month, year } = res;
    this.setState({ year2: year, month2: month, date2: date });
  }
  onResetDefaultDate2(res) {
    const { date, month, year } = res;
    this.setState({ year2: year, month2: month, date2: date });
  }
  componentWillUnmount = () => {
    document.removeEventListener("keypress", this.onenter);
  };
  componentDidMount = () => {
    document.addEventListener("keypress", this.onenter);
  };
  onenter = target => {
    if (target.charCode == 13) {
      this.onClick();
    }
  };
  onClick = async () => {
    if (
      this.state.year === "" ||
      this.state.year2 === "" ||
      this.state.month === "" ||
      this.state.month2 === "" ||
      this.state.date === "" ||
      this.state.date2 === ""
    ) {
      swal("Warning!", "Please complete the information", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => {});
    } else {
      let date_start =
        this.state.year + "-" + this.state.month + "-" + this.state.date;
      let date_end =
        this.state.year2 + "-" + this.state.month2 + "-" + this.state.date2;
      let empID=this.state.empID
      let obj = {
        empID,
        date_start,
        date_end
      };
      console.log(JSON.stringify(obj));
      try {
        let res = await post("/report_flexible", obj);
        this.setState({ dataTables: res.result });
        swal("Complete", " ", "success", {
          buttons: false,
          timer: 1200
        }).then(() => {
          window.location.reload();
        });
      } catch (error) {
        swal("Warning!", " ", "warning", {
          buttons: false,
          timer: 1200
        }).then(() => { });
      }
    }
  };
  render() {
    return (
      <div className="csrf1">
        <div className="csrf2">
          <div className="csrf3">
            <div> report flexible</div>
            <div>
                <input
                  id="confirm-btn"
                  name="Export"
                  type="button"
                  value="Export"
                  className="csrfbt"
                  onClick={this.onClick}
                />
            </div>
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-evenly",
              alignItems: "center"
            }}
          >
            <div>
              {" "}
              <PickyDateTime
                size="s"
                mode={0}
                locale="en-us"
                show={true}
                onYearPicked={res => this.onYearPicked(res)}
                onMonthPicked={res => this.onMonthPicked(res)}
                onDatePicked={res => this.onDatePicked(res)}
                onResetDate={res => this.onResetDate(res)}
                onResetDefaultDate={res => this.onResetDefaultDate(res)}
              />
            </div>
            <div>
              {" "}
              <PickyDateTime
                size="s"
                mode={0}
                locale="en-us"
                show={true}
                onYearPicked={res => this.onYearPicked2(res)}
                onMonthPicked={res => this.onMonthPicked2(res)}
                onDatePicked={res => this.onDatePicked2(res)}
                onResetDate={res => this.onResetDate2(res)}
                onResetDefaultDate={res => this.onResetDefaultDate2(res)}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
