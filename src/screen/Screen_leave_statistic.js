import React, { Component } from "react";
import "../css/Screen_leave_statistic.css";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend
} from "recharts";

export default class Page_leave_statistic extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataTables: [
        {
          leaID_type: "1",
          leaName_type: "LaYed",
          leaName_thai: "ลาเยส",
          already_leave: "10",
          max_day: "99"
        },
        {
          leaID_type: "1",
          leaName_type: "LaYed",
          leaName_thai: "ลาเยส",
          already_leave: "70",
          max_day: "88"
        },
        {
          leaID_type: "1",
          leaName_type: "LaYed",
          leaName_thai: "ลาเยส",
          already_leave: "0",
          max_day: "55"
        },
        {
          leaID_type: "1",
          leaName_type: "LaYed",
          leaName_thai: "ลาเยส",
          already_leave: "70",
          max_day: "88"
        },
        {
          leaID_type: "1",
          leaName_type: "LaYed",
          leaName_thai: "ลาเยส",
          already_leave: "0",
          max_day: "55"
        }
      ]
    };
  }
  renderSizePerPageDropDown = props => {
    return (
      <div className="btn-group">
        {[10, 25, 30, 40, 50, 100].map((n, idx) => {
          const isActive = n === props.currSizePerPage ? "active" : null;
          return (
            <button
              key={idx}
              type="button"
              className={`btn btn-info ${isActive}`}
              onClick={() => props.changeSizePerPage(n)}
            >
              {n}
            </button>
          );
        })}
      </div>
    );
  };
  _format = () => {};
  render() {
    return (
      <div className="csls1">
        <div className="csls2">
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              marginBottom: "15px"
            }}
          >
            <BarChart
              width={750}
              height={350}
              data={this.state.dataTables}
              stroke="lightcyan"
            >
              <CartesianGrid strokeDasharray="3 3" stroke="lightcyan" />
              <XAxis dataKey="leaName_type" stroke="lightcyan" />
              <YAxis stroke="lightcyan" />
              <Tooltip />
              <Legend />
              <Bar
                dataKey="already_leave"
                fill="#8884d8"
                dataFormat={this._format}
              />
              <Bar dataKey="max_day" fill="#82ca9d" />
            </BarChart>
          </div>
          <div>
            <BootstrapTable
              search
              className="utb"
              data={this.state.dataTables}
              pagination
              bordered={false}
              striped
              options={{
                sizePerPageDropDown: this.renderSizePerPageDropDown
              }}
            >
              <TableHeaderColumn
                width="80"
                dataField="leaID_type"
                dataSort
                isKey
              >
                {"ID"}
              </TableHeaderColumn>
              <TableHeaderColumn dataField="leaName_type" dataSort>
                {"Name English"}
              </TableHeaderColumn>
              <TableHeaderColumn dataField="leaName_thai" dataSort>
                {"Name Thai"}
              </TableHeaderColumn>
              <TableHeaderColumn width="150" dataField="already_leave" dataSort>
                {"You already leave"}
              </TableHeaderColumn>
              <TableHeaderColumn width="100" dataField="max_day" dataSort>
                {"Max Day"}
              </TableHeaderColumn>
            </BootstrapTable>
          </div>
        </div>
      </div>
    );
  }
}
