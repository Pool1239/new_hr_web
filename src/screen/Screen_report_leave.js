import React, { Component } from "react";
import "../css/Screen_report_leave.css";
import PickyDateTime from "react-picky-date-time";
import swal from "sweetalert";
import { post, get } from "../service/api";
// import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

export default class Screen_report_leave extends Component {
  constructor(props) {
    super(props);
    this.state = {
      empID: localStorage.getItem("empID"),
      showPickyDateTime: true,
      date: "",
      month: "",
      year: "",
      date2: "",
      month2: "",
      year2: "",
      dataTables: [],
      leaID: "",
      full_name: "",
      leaName_type: "",
      leaName_day: "",
      start: "",
      end: "",
      lea_reason: "",
      sta_name: "",
      created: "",
      totalall: ""

    };
  }

  onYearPicked(res) {
    const { year } = res;
    this.setState({ year: year });
  }
  onMonthPicked(res) {
    const { month, year } = res;
    this.setState({ year: year, month: month });
  }
  onDatePicked(res) {
    const { date, month, year } = res;
    this.setState({ year: year, month: month, date: date });
  }
  onResetDate(res) {
    const { date, month, year } = res;
    this.setState({ year: year, month: month, date: date });
  }
  onResetDefaultDate(res) {
    const { date, month, year } = res;
    this.setState({ year: year, month: month, date: date });
  }
  onYearPicked2(res) {
    const { year } = res;
    this.setState({ year2: year });
  }
  onMonthPicked2(res) {
    const { month, year } = res;
    this.setState({ year2: year, month2: month });
  }
  onDatePicked2(res) {
    const { date, month, year } = res;
    this.setState({ year2: year, month2: month, date2: date });
  }
  onResetDate2(res) {
    const { date, month, year } = res;
    this.setState({ year2: year, month2: month, date2: date });
  }
  onResetDefaultDate2(res) {
    const { date, month, year } = res;
    this.setState({ year2: year, month2: month, date2: date });
  }
  componentWillUnmount = () => {
    document.removeEventListener("keypress", this.onenter);
  };
  componentDidMount = () => {
    document.addEventListener("keypress", this.onenter);
  };
  onenter = target => {
    if (target.charCode == 13) {
      this.onClick();
    }
  };

  onClick = async () => {
    if (
      this.state.year === "" ||
      this.state.year2 === "" ||
      this.state.month === "" ||
      this.state.month2 === "" ||
      this.state.date === "" ||
      this.state.date2 === ""
    ) {
      swal("Warning!", "Please complete the information", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => { });
    } else {
      let date_start =
        this.state.year + "-" + this.state.month + "-" + this.state.date;
      let date_end =
        this.state.year2 + "-" + this.state.month2 + "-" + this.state.date2;
      let obj = {
        empID: this.state.empID,
        date_start,
        date_end
      };
      console.log(JSON.stringify(obj));
      try {
        let res = await post("/report_leave", obj);
        this.setState({ dataTables: res.result });
        swal("Complete", " ", "success", {
          buttons: false,
          timer: 1200
        }).then(() => {
          window.location.reload();
        });
      } catch (error) {
        swal("Warning!", " ", "warning", {
          buttons: false,
          timer: 1200
        }).then(() => { });
      }
    }
  };
  render() {

    return (
      <div className="csrl1">
        <div className="csrl2">
          <div className="csrl3">
            <div> report leave</div>
            <div>
              <input
                id="confirm-btn"
                name="Export"
                type="button"
                value="Export"
                className="csrbt"
                onClick={this.onClick}
              />
            </div>
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-evenly",
              alignItems: "center"
            }}
          >
            <div>
              {" "}
              <PickyDateTime
                size="s"
                mode={0}
                locale="en-us"
                show={true}
                onYearPicked={res => this.onYearPicked(res)}
                onMonthPicked={res => this.onMonthPicked(res)}
                onDatePicked={res => this.onDatePicked(res)}
                onResetDate={res => this.onResetDate(res)}
                onResetDefaultDate={res => this.onResetDefaultDate(res)}
              />
            </div>
            <div>
              {" "}
              <PickyDateTime
                size="s"
                mode={0}
                locale="en-us"
                show={true}
                onYearPicked={res => this.onYearPicked2(res)}
                onMonthPicked={res => this.onMonthPicked2(res)}
                onDatePicked={res => this.onDatePicked2(res)}
                onResetDate={res => this.onResetDate2(res)}
                onResetDefaultDate={res => this.onResetDefaultDate2(res)}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
