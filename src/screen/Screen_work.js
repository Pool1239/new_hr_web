import React, { Component } from "react";
import moment from "moment";
import {
  BootstrapTable,
  TableHeaderColumn,
  ButtonGroup
} from "react-bootstrap-table";
import "../css/Screen_user.css";
import "../css/Screen_work.css";
import Modal from "react-responsive-modal";
import brea from "../img/break.png";
import checkin from "../img/checkin.png";
import checkout from "../img/checkout.png";
import chart from '../img/chart.png'
import {
  ResponsiveContainer, LineChart, ComposedChart, Line, Area, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';
import schedule from 'node-schedule'
import cron from 'node-cron'
import { post, get } from "../service/api";
import swal from "sweetalert"

export default class Screen_work extends Component {
  constructor(props) {
    super(props);
    this.state = {
      empID: localStorage.getItem("empID"),
      checkin_id: [],
      date: "",
      date_in: "",
      date_out: "",
      time_in_break: "",
      time_out_break: "",
      full_name: "",
      comname: "",
      cal_date: "",

      month: "",
      year: "",

      checkTime: 0,
      buttonStatus: 0,
      disabled: false,
      dataTables: [],
      dataCharts: [],
      open: false,
      openW: false
    };
  }
  componentDidMount = async () => {
    try {
      let res = await get("/alldata_work");
      console.log("res", res.result);

      this.setState({ dataTables: res.result });

    } catch (error) {
      alert(error);
    }
  };
  componentWillMount = () => {
    this.renderCron();
  }

  // componentDidMount = () => {
  //   cron.schedule('1,2,4,5* * * * * *', function(){
  //     console.log('running every minute 1, 2, 4 and 5');
  // });

  // }

  // axis() {
  //   const {
  //     x, y, stroke, payload,
  //   } = this.props;

  //   return (
  //     <g transform={`translate(${x},${y})`}>
  //       <text x={0} y={0} dy={16} textAnchor="end" fill="#666" transform="rotate(-35)">{payload.value}</text>
  //     </g>
  //   );
  // }
  // label() {
  //   const {
  //     x, y, stroke, value,
  //   } = this.props;

  //   return <text x={x} y={y} dy={-4} fill={stroke} fontSize={10} textAnchor="middle">{value}</text>;
  // }

  renderCron() {
    cron.schedule('0-59 * * * * *', () => {
      // console.log('VVVVVVVVVVVVVVV');
      // console.log(Number(moment().format('HH')));

      let time = Number(moment().format('HH'));
      if (time >= 0 && time < 12) {
        this.setState({ checkTime: 1, buttonStatus: 0 })
      } else if (time === 12) {
        this.setState({ checkTime: 1, buttonStatus: 1 })
      } else if (time >= 13 && time < 18) {
        this.setState({ checkTime: 1, buttonStatus: 2 })
      } else if (time >= 18 && time < 24) {
        this.setState({ checkTime: 1, buttonStatus: 3 })
      }
    });
  }

  renderSizePerPageDropDown = props => {
    return (
      <div className="btn-group">
        {[10, 25, 30, 40, 50, 100].map((n, idx) => {
          const isActive = n === props.currSizePerPage ? "active" : null;

          return (
            <button
              key={idx}
              type="button"
              className={`btn btn-info ${isActive}`}
              onClick={() => props.changeSizePerPage(n)}
            >
              {n}
            </button>
          );
        })}
      </div>
    );
  };
  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  onChart = async () => {
    if (this.state.year == "") {
      swal("Warning!", "Please fill in this year form", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => {
        window.location.reload();
      });
    } else {
      let obj = {
        empID: this.state.empID,
        month: this.state.month,
        year: this.state.year
      };
      try {
        let chart = await post("/chartw", obj);
        console.log("chart", chart.result);

        this.setState({ dataCharts: chart.result });
        this.setState({ openW: true });

        // if (res.success === true) {
        //   localStorage.setItem("empID", res.result);
        //   this.props.history.push("/");
        // } else {
        //   swal(res.message, " ", "warning", {
        //     buttons: false,
        //     timer: 1200
        //   });
        // }
      } catch (error) {
        alert(error);
      }
    }
  };
  onOpenModal = () => {
    console.log("NOT DISABLED")
    this.setState({ open: true });
  };
  onCloseModal = () => {
    console.log("DISABLED")
    this.setState({ open: false });
  };
  onOpenModalW = () => {
    this.setState({ openW: true });
  };
  onCloseModalW = () => {
    this.setState({ openW: false });
  };


  createCustomButtonGroup = () => {
    let { checkTime, buttonStatus } = this.state;
    // let check = null;
    // let res = localStorage.getItem("buttonStatus")

    // console.log('VVVVVVVVVVVVVVV');
    //  cron.schedule('0-59 * * * * * *',  () => {
    // cron.schedule('* 0-59 0-11 * * 1-5',  () => {
    // console.log('VVVVVVVVVVVVVVV');
    // this.setState({checkTime:1})
    // this.setState({buttonStatus:0})
    // check =1
    // console.log(check,checkTime)
    //   return (
    //     <div>
    //       <label>A</label>
    //       <ButtonGroup className="my-custom-class" sizeClass="btn-group-md">
    //         <button
    //           type="button"
    //           className="csw1"
    //           onClick={this.onCheckin}
    //         >
    //           <img src={checkin} style={{ width: "15px", marginRight: "2px" }} />
    //           <div>CHECK IN</div>
    //         </button>
    //       </ButtonGroup>
    //     </div>
    //   )
    //  });
    //  cron.schedule('* 0-59 12 * * 1-5',  () => {
    //   this.setState({checkTime:1})
    //   this.setState({buttonStatus:1})
    //  })
    //  cron.schedule('* 0-59 13-17 * * 1-5',  () => {
    //   this.setState({checkTime:1})
    //   this.setState({buttonStatus:2})
    //  })
    //  cron.schedule('* 0-59 18-23 * * 1-5',  () => {
    //   this.setState({checkTime:1})
    //   this.setState({buttonStatus:3})
    //  })
    //  console.log("checktime",checkTime);  
    let { disabled, open } = this.state;
    // console.log('VVVVVVVVVVVVVVV');
    return (


      <div>
        {
          checkTime === 1 && buttonStatus === 0 ?
            <ButtonGroup className="my-custom-class" sizeClass="btn-group-md">
              <button
                id="button1"
                type="button"
                className="csw1"
                onClick={() => this.onCheckin()}
                disabled={disabled}
              >
                <img src={checkin} style={{ width: "15px", marginRight: "2px" }} />
                <div>CHECK IN</div>
              </button>
            </ButtonGroup>
            : checkTime === 1 && buttonStatus === 1 ?
              <ButtonGroup className="my-custom-class" sizeClass="btn-group-md">
                <button
                  id="button2"
                  type="button"
                  className="csw2"
                  onClick={() => this.onBreakin()}
                  disabled={disabled}
                >
                  <img src={brea} style={{ width: "15px", marginRight: "2px" }} />
                  <div>TIME IN BREAK</div>
                </button>
              </ButtonGroup>
              : checkTime === 1 && buttonStatus === 2 ?
                <ButtonGroup className="my-custom-class" sizeClass="btn-group-md">
                  <button
                    id="button3"
                    type="button"
                    className="csw3"
                    onClick={() => this.onBreakout()}
                    disabled={disabled}
                  >
                    <img src={brea} style={{ width: "15px", marginRight: "2px" }} />
                    <div>TIME OUT BREAK</div>
                  </button>
                </ButtonGroup>
                : checkTime === 1 && buttonStatus === 3 ?
                  <ButtonGroup className="my-custom-class" sizeClass="btn-group-md">
                    <button
                      id="button4"
                      type="button"
                      className="csw4"
                      onClick={() => this.onCheckout()}
                      disabled={disabled}
                    >
                      <img src={checkout} style={{ width: "15px", marginRight: "2px" }} />
                      <div>CHECK OUT</div>
                    </button>
                  </ButtonGroup>
                  : <div />
        }

        <ButtonGroup className="my-custom-class" sizeClass="btn-group-md">
          <button
            id="button5"
            type="button"
            className="csw-chart"
            // style={{marginLeft:10}}
            onClick={this.onOpenModal}

          >
            <img src={chart} style={{ width: "18px", marginRight: "2px" }} />
            <div>CHART</div>
          </button>
        </ButtonGroup>
      </div>
    );
  };
  onCheckin = async () => {
    this.setState({ disabled: true });
    let empID = this.state.empID;
    // alert(empID)
    try {
      let res = await post("/checkin", { empID });
      swal("CHECK IN", {
        buttons: false,
        timer: 1200
      }).then(() => {
        window.location.reload();
      });
    } catch (error) {
      alert(error);
    }
    // localStorage.setItem("timeCheckin","eiei")
    // localStorage.setItem("buttonStatus",'1')
  }
  onBreakin = async () => {
    this.setState({ disabled: true })
    let empID = this.state.empID;
    // alert(empID)
    try {
      let res = await post("/inbreak", { empID });
      swal("TIME IN BREAK", {
        buttons: false,
        timer: 1200
      }).then(() => {
        window.location.reload();
      });
    } catch (error) {
      alert(error);
    }
  }
  onBreakout = async () => {
    this.setState({ disabled: true })
    let empID = this.state.empID;
    // alert(empID)
    try {
      let res = await post("/outbreak", { empID });
      swal("TIME OUT BREAK", {
        buttons: false,
        timer: 1200
      }).then(() => {
        window.location.reload();
      });
    } catch (error) {
      alert(error);
    }
  }
  onCheckout = async () => {
    this.setState({ disabled: true })
    let empID = this.state.empID;
    // alert(empID)
    try {
      let res = await post("/checkout", { empID });
      swal("CHECK OUT", {
        buttons: false,
        timer: 1200
      }).then(() => {
        window.location.reload();
      });
    } catch (error) {
      alert(error);
    }
  }
  static jsfiddleUrl = 'https://jsfiddle.net/alidingling/7j5bbbum/';
  render() {
    // console.log("empID", this.state.empID);
    // console.log("work_id", this.state.checkTime);
    const { open, openW } = this.state;

    return (
      <div className="cu1">
        <div className="cu2">
          <div className="cu3">

            {" "}
            <div style={{ display: "flex" }}>
              <div>work</div>{" "}
            </div>
            {/* <div>6 works</div>{" "} */}
          </div>
          <BootstrapTable
            search
            className="utb"
            data={this.state.dataTables}
            pagination
            options={{
              btnGroup: this.createCustomButtonGroup,
              sizePerPageDropDown: this.renderSizePerPageDropDown
            }}
            bordered={false}
            striped
            cellEdit={{
              mode: "click"
            }}
            onChange={this.onChangeUpdate}
          >
            <TableHeaderColumn width="80" dataField="checkin_id" dataSort isKey>
              {"ID"}
            </TableHeaderColumn>
            <TableHeaderColumn width="140"
              dataField="date"
              dataFormat={cell => (
                <p>{moment(cell).format("L")}</p>
              )}
              dataSort
              editable={false}>
              {"Date"}
            </TableHeaderColumn>
            <TableHeaderColumn width="140"
              dataField="date_in"

              dataSort
              editable={false}>
              {"Check In"}
            </TableHeaderColumn>
            <TableHeaderColumn width="140"
              dataField="time_in_break"
              dataSort
              editable={false}>
              {"In Break"}
            </TableHeaderColumn>
            <TableHeaderColumn width="140"
              dataField="time_out_break"
              dataSort
              editable={false}>
              {"Out Break"}
            </TableHeaderColumn>
            <TableHeaderColumn width="140"

              dataField="date_out"
              dataSort
              editable={false}>
              {"Check Out"}
            </TableHeaderColumn>

            <TableHeaderColumn
              width="80"
              dataField="full_name"
              dataSort
              editable={false}
            >
              {"Name"}
            </TableHeaderColumn>
            <TableHeaderColumn
              width="80"
              dataField="comname"
              dataSort
              editable={false}
            >
              {"Company"}
            </TableHeaderColumn>

          </BootstrapTable>
        </div>
        <Modal open={open} onClose={this.onCloseModal}>
          <div
            style={{
              textTransform: "uppercase",
              marginTop: "10px",
              marginBottom: "15px",
              color: "lightcyan",
              fontSize: "large",
              width: "190px"
            }}
          >
            Enter Month And Year
          </div>
          <div>
            <input
              type="text"
              placeholder="MM"
              id="work_month"
              name="month"
              onChange={this.onChange}
              maxLength="2"
              autocorrect="off"
              autocapitalize="off"
            // className="cl5-1"
            />
          </div>
          <div>
            <input
              type="text"
              placeholder="YYYY"
              id="work_year"
              name="year"
              onChange={this.onChange}
              maxLength="4"
              autocorrect="off"
              autocapitalize="off"
            />
          </div>
          <div>
            <input
              type="button"
              value="SEARCH"
              className="cswb"
              onClick={() => this.onChart()}
            />
          </div>


        </Modal>
        <Modal open={openW} onClose={this.onCloseModalW} >
          <div
            style={{
              textTransform: "uppercase",
              marginTop: "10px",
              marginBottom: "15px",
              color: "lightcyan",
              fontSize: "large",
              width: "190px",

            }}
          >
            Chart
          </div>

          <div style={{ width: '750px', height: 300 }}>
            <ResponsiveContainer>
              <ComposedChart
                width={500}
                height={400}
                data={this.state.dataCharts}
                margin={{
                  top: 20, right: 50, bottom: 20, left: 0,
                }}
              >
                <CartesianGrid strokeDasharray="4 4" stroke="lightcyan" />
                <XAxis dataKey="Date" stroke="lightcyan" />
                <YAxis stroke="lightcyan" />
                <Tooltip />
                <Legend />
                {/* <Area type="monotone" dataKey="WorkHour" fill="#009432" stroke="#009432" /> */}
                <Bar dataKey="WorkHour" barSize={20} fill="#009432" />
                <Bar dataKey="BreakHour" barSize={20} fill="#413ea0" />
                {/* <Line type="monotone" dataKey="cal_date" stroke="#A3CB38" /> */}
              </ComposedChart>

              {/* <LineChart
                width={500}
                height={300}
                data={this.state.dataCharts}
                margin={{
                  top: 20, right: 30, left: 20, bottom: 10,
                }}
              >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" height={60} tick={this.axis()} />
                <YAxis />
                <Tooltip />
                <Legend />  
                <Line type="monotone" dataKey="pv" stroke="#8884d8" label={this.label()} />
                <Line type="monotone" dataKey="uv" stroke="#82ca9d" />
              </LineChart> */}
            </ResponsiveContainer>
          </div>


        </Modal>

      </div>
    )
  }
}
