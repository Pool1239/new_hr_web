import React, { Component } from "react";
import swal from "sweetalert";
import moment from "moment";
import "../css/Screen_myflexiblerequest.css";
import PickyDateTime from "react-picky-date-time";
import FileBase64 from "../lib/react-file-base64/src/js/components/react-file-base64";
import insert from "../img/insert.png";
import Modal from "react-responsive-modal";
import { post, get } from "../service/api";
var x = [1];
var data = [];

export default class Screen_myflexiblerequest extends Component {
  constructor(props) {
    super(props);

    this.state = {
      empID: localStorage.getItem("empID"),
      row: [],
      files: [],
      benID_type: "",
      benName_type: "",
      amount: "",
      showPickyDateTime: true,
      date: "",
      month: "",
      year: "",
      dataTables: []
    };
  }
  componentDidMount = async () => {
    try {
      let res = await get("/show_my_flexible_request");
      console.log("res", res.result);

      this.setState({ dataTables: res.result });

    } catch (error) {
      alert(error);
    }
  };
  componentWillMount = () => {
    this.getRow();
  };
  state = {
    open: false
  };
  onOpenModal = () => {
    this.setState({ open: true });
  };
  onCloseModal = () => {
    this.setState({ open: false });
    window.location.reload();
  };
  getFiles(files) {
    this.setState({
      files: files,
      imagePreviewUrl: files[0].base64
    });
    console.log("VVS", files[0].base64);
  }
  onYearPicked(res) {
    const { year } = res;
    this.setState({ year: year });
  }
  onMonthPicked(res) {
    const { month, year } = res;
    this.setState({ year: year, month: month });
  }
  onDatePicked(res) {
    const { date, month, year } = res;
    this.setState({ year: year, month: month, date: date });
  }
  onResetDate(res) {
    const { date, month, year } = res;
    this.setState({ year: year, month: month, date: date });
  }
  onResetDefaultDate(res) {
    const { date, month, year } = res;
    this.setState({ year: year, month: month, date: date });
  }
  onClick = async element => {
    this.setState({
      benName_type: element.benName_type,
      benID_type: element.benID_type
    });
  };
  onSubmit = async () => {
    let ben_type = this.state.benID_type
    let ben_date_receipt = this.state.year + "-" + this.state.month + "-" + this.state.date
    let amount = this.state.amount
    let empID = this.state.empID
    let files = this.state.files

    // console.log("IN", {
    //   ben_type,
    //   ben_date_receipt,
    //   amount,
    //   empID,
    //   files : files[0].base64
    // });
    let obj = {
      empID,
      ben_type,
      ben_date_receipt,
      amount,
      files : files[0].base64
    }
    console.log(JSON.stringify(obj));
    
    try {
      let res = await post("/insert_my_flexible_benefit", obj);
      if (res.success === true) {
        swal("Complete", " ", "success", {
          buttons: false,
          timer: 1200
        }).then(() => {
          window.location.reload();
        });
      } else {
        swal("Fail", " ", "warning", {
          buttons: false,
          timer: 1200
        }).then(() => { });
      }
    } catch (error) {
      swal(error, " ", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => { });
    }
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onChangeCreate = (e, i) => {
    data[i] = {
      ...data[i],
      [e.target.name]: e.target.value,
      IDcompany: this.state.dataTables[0].IDcompany,
      empID: this.state.empID
    };
    console.log("Create", data);
  };
  onClickInsertCreate = async () => {
    x.push(1);
    this.getRow();
  };
  onClickCreate = async i => {
    const a = data;
    let benName_type = [];
    let IDcompany = [];
    for (let i = 0; i < a.length; i++) {
      benName_type.push(a[i].benName_type);
      IDcompany.push(a[i].IDcompany);
    }
    const obj = {
      empID: this.state.empID,
      benName_type,
      IDcompany
    };
    console.log("obj", obj);
    try {
      await post('/create_my_flexible_request', obj)
      swal("Complete", " ", "success", {
        buttons: false,
        timer: 1200
      }).then(() => {
        window.location.reload();
      });
      window.location.reload()
    } catch (error) {
      swal("Fail", " ", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => { });
    }
  };
  getRow() {
    let row = x.map((ele, i) => {
      return this.renderRow(i);
    });
    this.setState({ row });
  }
  renderRow(i) {
    return (
      <div>
        <text style={{ marginRight: 5, color: "lightcyan" }}>{i + 1}.</text>
        <input
          type="text"
          name="benName_type"
          className="cmdc2"
          onChange={e => this.onChangeCreate(e, i)}
        />
      </div>
    );
  }
  render() {
    const { open, row } = this.state;
    console.log("file64", this.state.files);
    let { imagePreviewUrl } = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = <img className="imgUpload" src={imagePreviewUrl} />;
    } else {
      $imagePreview = <div className="previewText" />;
    }
    return (
      <div className="cfr1">
        <div className="cfr2">
          <div className="cfr3">
            <div className="cfr3-1">my flexible request</div>
            <div>
              <button
                type="button"
                className="cfr3-2"
                onClick={this.onOpenModal}
              >
                <img
                  src={insert}
                  style={{ width: "15px", marginRight: "2px" }}
                />
                <div>CREATE</div>
              </button>
            </div>
          </div>
          <div className="cfr4">
            <div className="cfr5">
              {this.state.dataTables.map(element => {
                return (
                  <div className="cfr11">
                    <div>___</div>
                    <button
                      className="cfr12"
                      onClick={() => this.onClick(element)}
                    >
                      <div className="cfr12-1">
                        <div>{element.benName_type}</div>
                      </div>
                      <div>></div>
                    </button>
                  </div>
                );
              })}
            </div>
            <div className="cfr6">
              <PickyDateTime
                size="s"
                mode={0}
                locale="en-us"
                show={true}
                onYearPicked={res => this.onYearPicked(res)}
                onMonthPicked={res => this.onMonthPicked(res)}
                onDatePicked={res => this.onDatePicked(res)}
                onResetDate={res => this.onResetDate(res)}
                onResetDefaultDate={res => this.onResetDefaultDate(res)}
              />
            </div>
            <div className="cfr7">
              <div className="cfr8">
                {$imagePreview}
                <div
                  style={{ textAlign: "-webkit-center", color: "lightcyan" }}
                >
                  <FileBase64
                    multiple={true}
                    onDone={this.getFiles.bind(this)}
                  />{" "}
                </div>
              </div>
            </div>
            <div className="cfr15">
              <div className="cfr9">
                <div style={{ color: "lightcyan", marginBottom: "2px" }}>
                  Please Enter Amount
                </div>
                <input
                  name="amount"
                  type="number"
                  placeholder="0.00"
                  className="cfr13"
                  onChange={this.onChange}
                />
              </div>
              <div className="cfr14">
                <div>
                  {this.state.benName_type === "" ? (
                    <div>Please Select Flexible!!!</div>
                  ) : (
                      this.state.benName_type
                    )}
                </div>
                <div>
                  {this.state.year === "" ? (
                    <div>Please Select Date!!!</div>
                  ) : (
                      this.state.date +
                      "-" +
                      this.state.month +
                      "-" +
                      this.state.year
                    )}
                </div>
              </div>
              <div className="cfr10">
                <input
                  id="confirm-btn"
                  name="sunmit"
                  type="button"
                  value="submit"
                  className="cbtns"
                  onClick={this.onSubmit}
                />
              </div>
            </div>
          </div>
        </div>
        <Modal open={open} onClose={this.onCloseModal}>
          <div>
            <div className="cmdc">CREATE PERPOSE</div>
          </div>
          <div
            style={{
              width: "100%",
              display: "flex",
              justifyContent: "space-around",
              alignItems: "center",
              flexDirection: "row",
              color: "lightcyan"
            }}
          >
            <div style={{ width: '7%' }} />
            <div style={{ width: '96%' }}>Perpose Name</div>
          </div>
          {row}
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                width: "94%"
              }}
            >
              <button onClick={this.onClickInsertCreate} className="cmdc3">
                เพิ่มช่อง
              </button>
              <button onClick={this.onClickCreate} className="cmdc3">
                เพิ่มข้อมูล
              </button>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}
