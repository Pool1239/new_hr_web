import React, { Component } from "react";
import moment from "moment";
import {
  BootstrapTable,
  TableHeaderColumn,
  ButtonGroup
} from "react-bootstrap-table";
import "../css/Screen_user.css";
import Modal from "react-responsive-modal";
import swal from "sweetalert";
import { FormControl, FormGroup, ControlLabel } from "react-bootstrap";
import insert from "../img/insert.png";
import update from "../img/update.png";
import del from "../img/del.png";
import { object } from "prop-types";
import { post, get } from "../service/api";

let dumpResEmail = [];
let dumpResUsername = [];
let special = /[*+$^[\]{};'"\\|<>#]/;

export default class Screen_user extends Component {
  constructor(props) {
    super(props);

    this.state = {
      empID: [],
      userName: "",
      emp_name: "",
      emp_last: "",
      email: "",
      pro_period: "",
      role: "",
      members: "",
      dataTables: [],
      dataTables2: []
    };
  }
  componentDidMount = async () => {
    try {
      let res = await get("/alldata_user");
      this.setState({
        dataTables: res.result.datas2,
        members: res.result.data3
      });
    } catch (error) {
      alert(error);
    }
  };
  onRowSelect = (row, isSelected) => {
    if (isSelected === true) {
      // console.log(`selected: ${isSelected}`);
      // console.log("row", row.empID);
      // let e = this.state.empID;
      // e.push(row.empID)
      // this.setState({empID: e}) //  [1.2.3] => empID: [1,2,3, row.empID]
      this.setState(prev => ({ empID: [...prev.empID, row.empID] }));
    } else {
      // console.log(`selected: ${isSelected}`);
      let e = this.state.empID; // [1,2,3] => [1,2,3].findIndex(el => 1,2,3)
      let index = e.findIndex(el => el === row.empID);
      e.splice(index, 1);
      this.setState({ empID: e });
    }
  };
  onSelectAll = isSelected => {
    // console.log(`is select all: ${isSelected}`);
    if (isSelected === true) {
      let e = this.state.dataTables.map(el => el.empID);
      this.setState({ empID: e });
    } else {
      this.setState({ empID: [] });
    }
  };
  _onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onChangeUpdate = e => {
    this.setState({ [e.target.dataField]: e.target.value });
  };
  state = {
    open: false
  };
  onOpenModal = () => {
    this.setState({ open: true });
  };
  onCloseModal = () => {
    this.setState({ open: false });
  };
  _onclick = async () => {
    if (
      this.state.userName == "" ||
      this.state.emp_name == "" ||
      this.state.emp_last == "" ||
      this.state.email == "" ||
      this.state.pro_period == "" ||
      this.state.role == ""
    ) {
      swal("Warning!", "Please fill out this form", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => {});
    } else {
      let res = localStorage.getItem("empID");
      let user_create_id = res;
      let company = 1;
      let userName = this.state.userName;
      let emp_name = this.state.emp_name;
      let emp_last = this.state.emp_last;
      let email = this.state.email;
      let pro_period = this.state.pro_period;
      let role = this.state.role;
      let obj = {
        userName,
        emp_name,
        emp_last,
        email,
        pro_period,
        role,
        user_create_id,
        company
      };
      try {
        let res = await post("/insert_user", obj);
        swal(res.result, " ", "success", {
          buttons: false,
          timer: 1200
        }).then(() => {
          this.onCloseModal();
          window.location.reload();
        });
      } catch (error) {
        swal("Warning!", " ", "warning", {
          buttons: false,
          timer: 1200
        }).then(() => {});
      }
    }
  };
  _status = cell => {
    if (cell === "4 months") return '<font color="red">' + cell + "</font>";
    else if (cell === "Pass") return '<font color="orange">' + cell + "</font>";
    else return cell;
  };
  getValidationAddFirstnameState() {
    const name = this.state.emp_name;
    const length = this.state.emp_name.length;
    if (length >= 1 && name != " ") return "success";
    else if (length >= 1 && name === " ") return "warning";
    else if (length === 0) return "error";
    return null;
  }
  getValidationAddLastnameState() {
    const name = this.state.emp_last;
    const length = this.state.emp_last.length;
    if (length >= 1 && name != " ") return "success";
    else if (length >= 1 && name === " ") return "warning";
    else if (length === 0) return "error";
    return null;
  }
  getValidationAddEmailState() {
    const name = this.state.email;
    const length = this.state.email.length;
    const searchemail = this.state.email.trim().toLowerCase();
    if (
      searchemail
      // && special.test(searchemail) === false
    ) {
      dumpResEmail = this.state.dataTables.filter(el => {
        return el.email.toLowerCase() === searchemail;
        // el.email.toLowerCase().match(searchemail);
      });
    }

    if (length >= 1 && name != " " && dumpResEmail.length === 0)
      return "success";
    else if (length >= 1 && name === " ") return "warning";
    else if (length === 0) return "error";
    else if (dumpResEmail) return "error";
    return null;
  }
  getValidationAddUsernameState() {
    const name = this.state.userName;
    const length = this.state.userName.length;
    const searcheusername = this.state.userName.trim().toLowerCase();
    if (
      searcheusername
      // && special.test(searchemail) === false
    ) {
      dumpResUsername = this.state.dataTables.filter(el => {
        return el.userName.toLowerCase() === searcheusername;
        // el.email.toLowerCase().match(searchemail);
      });
    }
    if (length >= 1 && name != " " && dumpResUsername.length === 0)
      return "success";
    else if (length >= 1 && name === " ") return "warning";
    else if (length === 0) return "error";
    else if (dumpResUsername) return "error";
    return null;
  }

  handleAddfirstname = e => {
    this.setState({ emp_name: e.target.value });
  };
  handleAddLastname = e => {
    this.setState({ emp_last: e.target.value });
  };
  handleAddEmail = e => {
    this.setState({ email: e.target.value });
  };
  handleAddUsername = e => {
    this.setState({ userName: e.target.value });
  };

  onClickUpdate = async () => {
    try {
      let res = await get("/alldata_user");
      this.setState({ dataTables2: res.result.datas2 });
    } catch (error) {
      alert(error);
    }
    let full_name = this.state.dataTables.map((el, i) => {
      const indexUpdate =
        JSON.stringify(this.state.dataTables2[i]) === JSON.stringify(el); //*** เปรียบเทียบ arry[1] กับ arry[2] เหมือนกันไหม */
      if (!indexUpdate) {
        return { ...el, find: true };
      } else {
        return { ...el, find: false };
      }
    });
    let fine = full_name.map(el => el.find);
    // console.log(fine);
    let index = fine.some(el => el == true);
    // console.log(index);
    if (!index) {
      swal("กรุณาทำการแก้ไขข้อมูล", " ", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => {});
    } else {
      let datas = full_name.map(el => {
        const spit = el.full_name.split(" ");
        return { ...el, emp_name: spit[0], emp_last: spit[1] };
      });
      try {
        let res = await post("/update_user", { datas });
        if (res.success === true) {
          swal(res.result, " ", "success", {
            buttons: false,
            timer: 1200
          }).then(() => {
            window.location.reload();
          });
        } else {
          swal(res.message, " ", "warning", {
            buttons: false,
            timer: 1200
          }).then(() => {});
        }
      } catch (error) {
        swal(error, " ", "warning", {
          buttons: false,
          timer: 1200
        }).then(() => {});
      }
    }
  };
  onDeleteRow = () => {
    if (this.state.empID.length === 0) {
      swal("กรุณาเลือกข้อมูล!", " ", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => {});
    } else {
      swal({
        title: "Are you sure?",
        text: "คุณแน่ใจนะ!",
        icon: "warning",
        buttons: true,
        dangerMode: true
      }).then(async willDelete => {
        if (willDelete) {
          let empID = this.state.empID;
          try {
            let res = await post("/delete_user", { empID });
            swal(res.result, " ", "success", {
              buttons: false,
              timer: 1200
            }).then(() => {
              window.location.reload();
            });
          } catch (error) {
            alert(error);
          }
        }
      });
    }
  };
  renderSizePerPageDropDown = props => {
    return (
      <div className="btn-group">
        {[10, 25, 30, 40, 50, 100].map((n, idx) => {
          const isActive = n === props.currSizePerPage ? "active" : null;
          return (
            <button
              key={idx}
              type="button"
              className={`btn btn-info ${isActive}`}
              onClick={() => props.changeSizePerPage(n)}
            >
              {n}
            </button>
          );
        })}
      </div>
    );
  };
  createCustomButtonGroup = () => {
    return (
      <div>
        <ButtonGroup className="my-custom-class" sizeClass="btn-group-md">
          <button
            type="button"
            className="cubt-insert"
            onClick={this.onOpenModal}
          >
            <img src={insert} style={{ width: "15px", marginRight: "2px" }} />
            <div>INSERT</div>
          </button>
        </ButtonGroup>
        <ButtonGroup className="my-custom-class" sizeClass="btn-group-md">
          <button
            type="button"
            className="cubt-update"
            onClick={this.onClickUpdate}
          >
            <img src={update} style={{ width: "15px", marginRight: "2px" }} />
            <div>UPDATE</div>
          </button>
        </ButtonGroup>
        <ButtonGroup className="my-custom-class" sizeClass="btn-group-md">
          <button
            type="button"
            className="cubt-delete"
            onClick={this.onDeleteRow}
          >
            <img src={del} style={{ width: "15px", marginRight: "2px" }} />
            <div>DELETE</div>
          </button>
        </ButtonGroup>
      </div>
    );
  };

  render() {
    // console.log("empID", this.state.empID);
    // console.log(dumpResEmail, this.state.email);
    const selectRowProp = {
      mode: "checkbox",
      clickToSelect: true,
      onSelect: this.onRowSelect,
      onSelectAll: this.onSelectAll
    };
    const { open } = this.state;
    return (
      <div className="cu1">
        <div className="cu2">
          <div className="cu3">
            {" "}
            <div style={{ display: "flex" }}>
              <div>user</div>{" "}
            </div>
            <div>{this.state.members} users</div>{" "}
          </div>
          <BootstrapTable
            search
            className="utb"
            data={this.state.dataTables}
            pagination
            options={{
              btnGroup: this.createCustomButtonGroup,
              sizePerPageDropDown: this.renderSizePerPageDropDown
            }}
            bordered={false}
            striped
            cellEdit={{
              mode: "click"
            }}
            onChange={this.onChangeUpdate}
            selectRow={selectRowProp}
          >
            <TableHeaderColumn
              width="80"
              dataField="empID"
              dataSort="empID"
              isKey
            >
              {"ID"}
            </TableHeaderColumn>
            <TableHeaderColumn dataField="full_name">
              {"Employee Name"}
            </TableHeaderColumn>
            <TableHeaderColumn dataField="email">{"Email"}</TableHeaderColumn>
            <TableHeaderColumn
              width="130"
              dataField="pro_name"
              dataFormat={this._status}
              editable={{
                type: "select",
                options: { values: ["4 months", "Pass"] }
              }}
            >
              {"Probation period"}
            </TableHeaderColumn>
            <TableHeaderColumn
              width="140"
              dataField="rol_name"
              // dataFormat={this.position}
              editable={{
                type: "select",
                options: { values: ["Employee", "Admin", "SuperAdmin"] }
              }}
            >
              {"Role"}
            </TableHeaderColumn>
            <TableHeaderColumn
              width="175"
              dataField="starting_date"
              dataFormat={cell => (
                <p>{moment(cell).format("DD-MM-YYYY  HH:MM:SS")}</p>
              )}
              editable={false}
            >
              {"Starting Date"}
            </TableHeaderColumn>
            <TableHeaderColumn dataField="admin_email" editable={false}>
              {"Approval Email"}
            </TableHeaderColumn>
            <TableHeaderColumn width="150" dataField="comname" editable={false}>
              {"Company"}
            </TableHeaderColumn>
          </BootstrapTable>
        </div>
        <Modal open={open} onClose={this.onCloseModal} center>
          <div
            style={{
              textTransform: "uppercase",
              marginTop: "10px",
              marginBottom: "15px",
              color: "lightcyan",
              fontSize: "large"
            }}
          >
            Create user
          </div>
          <FormGroup
            controlId="formBasicText"
            validationState={this.getValidationAddFirstnameState()}
          >
            <FormControl
              type="text"
              name="emp_name"
              value={this.state.emp_name}
              placeholder="First Name"
              onChange={this.handleAddfirstname}
            />
            <FormControl.Feedback />
          </FormGroup>

          <FormGroup
            controlId="formBasicText"
            validationState={this.getValidationAddLastnameState()}
          >
            <FormControl
              type="text"
              name="emp_last"
              value={this.state.emp_last}
              placeholder="Last Name"
              onChange={this.handleAddLastname}
            />
            <FormControl.Feedback />
          </FormGroup>

          <FormGroup
            controlId="formBasicText"
            validationState={this.getValidationAddEmailState()}
          >
            <FormControl
              type="text"
              name="email"
              value={this.state.email}
              placeholder="ชื่ออีเมลล์@example.com"
              onChange={this.handleAddEmail}
            />
            <FormControl.Feedback />
            {dumpResEmail.length !== 0 ? (
              <ControlLabel>อีเมลล์ถูกใช้งานแล้ว</ControlLabel>
            ) : null}
          </FormGroup>

          <form className="cro">
            <div>
              <input
                type="radio"
                name="pro_period"
                value={1}
                onClick={() => this.setState({ radio: 1 })}
                style={{ marginRight: "5px" }}
                checked={this.state.radio === 1}
                onChange={this._onChange}
              />
              Pass
            </div>
            <div>
              <input
                type="radio"
                name="pro_period"
                value={2}
                onClick={() => this.setState({ radio: 2 })}
                style={{ marginRight: "5px" }}
                checked={this.state.radio === 2}
                onChange={this._onChange}
              />
              4 Month
            </div>
          </form>

          <FormGroup
            controlId="formBasicText"
            validationState={this.getValidationAddUsernameState()}
          >
            <FormControl
              type="text"
              name="userName"
              value={this.state.userName}
              placeholder="Username"
              onChange={this.handleAddUsername}
            />
            <FormControl.Feedback />
            {dumpResUsername.length !== 0 ? (
              <ControlLabel>ชื่อผู้ใช้ถูกใช้งานแล้ว</ControlLabel>
            ) : null}
          </FormGroup>

          <form className="cro">
            <div>
              <input
                type="radio"
                name="role"
                value={1}
                onClick={() => this.setState({ radio2: 1 })}
                style={{ marginRight: "5px" }}
                checked={this.state.radio2 === 1}
                onChange={this._onChange}
              />
              Admin
            </div>
            <div style={{ width: "70.75px" }}>
              <input
                type="radio"
                name="role"
                value={2}
                onClick={() => this.setState({ radio2: 2 })}
                style={{ marginRight: "5px" }}
                checked={this.state.radio2 === 2}
                onChange={this._onChange}
              />
              User
            </div>
          </form>
          <div>
            <input
              type="button"
              value="Accept"
              className="ubt2"
              onClick={() => this._onclick()}
            />
          </div>
        </Modal>
      </div>
    );
  }
}
