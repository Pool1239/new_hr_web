import React, { Component } from "react";
import moment from "moment";
import {
  BootstrapTable,
  TableHeaderColumn,
  ButtonGroup
} from "react-bootstrap-table";
import "../css/Screen_user.css";
import "../css/Screen_project.css"
import Modal from "react-responsive-modal";
import swal from "sweetalert";
import { FormControl, FormGroup, ControlLabel } from "react-bootstrap";
import insert from "../img/insert.png";
import update from "../img/update.png";
import chart from '../img/chart.png'
import del from "../img/del.png";
import { object } from "prop-types";
import PickyDateTime from "react-picky-date-time";
import {
  ResponsiveContainer, ComposedChart, Line, Area, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';
import { post, get } from "../service/api";

export default class Screen_user extends Component {
  constructor(props) {
    super(props);

    this.state = {
      empID: localStorage.getItem("empID"),
      project_id: [],
      project_name: "",
      project_start: "",
      project_end: "",
      company_id: "",
      comname: "",
      full_name: "",

      dataTables2: [],
      dataCharts: [],
      members: "",

      // project_name2: "",

      showPickyDateTime: true,
      date: "",
      month: "",
      year: "",
      date2: "",
      month2: "",
      year2: "",

      startDate: new Date(),

      dataTables: []
    };
  }
  componentDidMount = async () => {
    try {
      let res = await get("/alldata_project");
      // console.log("res", res.result);

      this.setState({
        dataTables: res.result.datas,
        members: res.result.datas2
      });

    } catch (error) {
      alert(error);
    }
  };
  onRowSelect = (row, isSelected) => {
    if (isSelected === true) {
      console.log(`selected: ${isSelected}`);
      console.log("row", row.project_id);
      this.setState(prev => ({ project_id: [...prev.project_id, row.project_id] }));
    } else {
      console.log(`selected: ${isSelected}`);
      let e = this.state.project_id; // [1,2,3] => [1,2,3].findIndex(el => 1,2,3)
      let index = e.findIndex(el => el === row.project_id);
      e.splice(index, 1);
      this.setState({ project_id: e });
    }
  };
  onSelectAll = isSelected => {
    console.log(`is select all: ${isSelected}`);
    if (isSelected === true) {
      let e = this.state.dataTables.map(el => el.project_id);
      this.setState({ project_id: e });
    } else {
      this.setState({ project_id: [] });
    }
  };
  _onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onChangeUpdate = e => {
    this.setState({ [e.target.dataField]: e.target.value });
  };
  state = {
    open: false
  };
  onOpenModal = () => {
    this.setState({ open: true });
  };
  onCloseModal = () => {
    this.setState({ open: false });
    window.location.reload();
  };
  onOpenModalC = () => {
    this.setState({ openC: true });
  };
  onCloseModalC = () => {
    this.setState({ openC: false });
  };
  onOpenModalG = () => {
    this.setState({ openG: true });
  };
  onCloseModalG = () => {
    this.setState({ openG: false, openC: true });
  };

  onChart = async () => {
    if (this.state.project_id == "") {
      swal("Warning!", "Please fill in this year form", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => {
        window.location.reload();
      });
    } else {
      let project_id = this.state.project_id

      try {
        let chart = await post("/chartp", { project_id });
        console.log("chart", chart.result);

        this.setState({ dataCharts: chart.result });
        this.setState({ openG: true });

        // if (res.success === true) {
        //   localStorage.setItem("empID", res.result);
        //   this.props.history.push("/");
        // } else {
        //   swal(res.message, " ", "warning", {
        //     buttons: false,
        //     timer: 1200
        //   });
        // }
      } catch (error) {
        alert(error);
      }
    }
  };

  _onclick = async () => {

    let empID = localStorage.getItem("empID");
    let project_name = this.state.project_name;
    let project_start = this.state.year + "-" + this.state.month + "-" + this.state.date;
    let project_end = this.state.year2 + "-" + this.state.month2 + "-" + this.state.date2;

    let obj = {
      empID,
      project_name,
      project_start,
      project_end
    };
    try {
      let res = await post("/insert_project", obj);
      swal("Complete", " ", "success", {
        buttons: false,
        timer: 1200
      }).then(() => {
        // this.onCloseModal();
        window.location.reload();
      });
    } catch (error) {
      alert(error)
    }

  };

  getValidationAddProjectnameState() {
    const name = this.state.project_name;
    const length = this.state.project_name.length;
    if (length >= 1 && name != " ") return "success";
    else if (length >= 1 && name === " ") return "warning";
    else if (length === 0) return "error";
    return null;
  }

  handleAddProjectname = e => {
    this.setState({ project_name: e.target.value });

  };

  onClickUpdate = async () => {
    // console.log("data", this.state.dataTables);
    try {
      let res = await get("/alldata_project");
      // console.log("data2",res.result.datas)
      this.setState({ dataTables2: res.result.datas });
    } catch (error) {
      alert(error);
    }
    let project_name = this.state.dataTables.map((el, i) => {
      const indexUpdate =
        JSON.stringify(this.state.dataTables2[i]) === JSON.stringify(el); //*** เปรียบเทียบ arry[1] กับ arry[2] เหมือนกันไหม */
      if (!indexUpdate) {
        return { ...el, find: true };
      } else {
        return { ...el, find: false };
      }
    });
    console.log("PN", project_name);

    let fine = project_name.map(el => el.find);
    // console.log(fine);
    let index = fine.some(el => el == true);
    console.log(index);
    if (index != true) {
      swal("กรุณาทำการแก้ไขข้อมูล", " ", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => { });
    } else {
      let datas = project_name
      try {
        let res = await post("/update_project", { datas });
        if (res.success === true) {
          swal(res.result, " ", "success", {
            buttons: false,
            timer: 1200
          }).then(() => {
            window.location.reload();
          });
        } else {
          swal(res.message, " ", "warning", {
            buttons: false,
            timer: 1200
          }).then(() => { });
        }
      } catch (error) {
        swal(error, " ", "warning", {
          buttons: false,
          timer: 1200
        }).then(() => { });
      }
    }
  };

  onDeleteRow = async () => {
    // console.log("Delete", this.state.project_id);
    if (this.state.project_id.length === 0) {
      swal("กรุณาเลือกข้อมูล!", " ", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => { });
    } else {
      swal({
        title: "Are you sure?",
        text: "คุณแน่ใจนะ!",
        icon: "warning",
        buttons: true,
        dangerMode: true
      }).then(async willDelete => {
        if (willDelete) {
          let project_id = this.state.project_id;
          try {
            let res = await post("/delete_project", { project_id });
            swal(res.result, " ", "success", {
              buttons: false,
              timer: 1200
            }).then(() => {
              window.location.reload();
            });
          } catch (error) {
            alert(error);
          }
        }
      });
    }
  };

  onYearPicked(res) {
    const { year } = res;
    this.setState({ year: year });
  }
  onMonthPicked(res) {
    const { month, year } = res;
    this.setState({ year: year, month: month });
  }
  onDatePicked(res) {
    const { date, month, year } = res;
    this.setState({ year: year, month: month, date: date });
  }
  onResetDate(res) {
    const { date, month, year } = res;
    this.setState({ year: year, month: month, date: date });
  }
  onResetDefaultDate(res) {
    const { date, month, year } = res;
    this.setState({ year: year, month: month, date: date });
  }
  onYearPicked2(res) {
    const { year } = res;
    this.setState({ year2: year });
  }
  onMonthPicked2(res) {
    const { month, year } = res;
    this.setState({ year2: year, month2: month });
  }
  onDatePicked2(res) {
    const { date, month, year } = res;
    this.setState({ year2: year, month2: month, date2: date });
  }
  onResetDate2(res) {
    const { date, month, year } = res;
    this.setState({ year2: year, month2: month, date2: date });
  }
  onResetDefaultDate2(res) {
    const { date, month, year } = res;
    this.setState({ year2: year, month2: month, date2: date });
  }
  renderSizePerPageDropDown = props => {
    return (
      <div className="btn-group">
        {[10, 25, 30, 40, 50, 100].map((n, idx) => {
          const isActive = n === props.currSizePerPage ? "active" : null;
          return (
            <button
              key={idx}
              type="button"
              className={`btn btn-info ${isActive}`}
              onClick={() => props.changeSizePerPage(n)}
            >
              {n}
            </button>
          );
        })}
      </div>
    );
  };

  createCustomButtonGroup = () => {
    return (
      <div>
        <ButtonGroup className="my-custom-class" sizeClass="btn-group-md">
          <button
            type="button"
            className="cubt-insert"
            onClick={this.onOpenModal}
          >
            <img src={insert} style={{ width: "15px", marginRight: "2px" }} />
            <div>INSERT</div>
          </button>
        </ButtonGroup>
        <ButtonGroup className="my-custom-class" sizeClass="btn-group-md">
          <button
            type="button"
            className="cubt-update"
            onClick={this.onClickUpdate}
          >
            <img src={update} style={{ width: "15px", marginRight: "2px" }} />
            <div>UPDATE</div>
          </button>
        </ButtonGroup>
        <ButtonGroup className="my-custom-class" sizeClass="btn-group-md">
          <button
            type="button"
            className="cubt-delete"
            onClick={this.onDeleteRow}
          >
            <img src={del} style={{ width: "15px", marginRight: "2px" }} />
            <div>DELETE</div>
          </button>
        </ButtonGroup>
        <ButtonGroup className="my-custom-class" sizeClass="btn-group-md">
          <button
            type="button"
            className="csp-chart"
            onClick={this.onOpenModalC}
          >
            <img src={chart} style={{ width: "18px", marginRight: "2px" }} />
            <div>CHART</div>
          </button>
        </ButtonGroup>
      </div>
    );
  };
  static jsfiddleUrl = '//jsfiddle.net/alidingling/9wnuL90w/';
  render() {
    console.log("project_id", this.state.project_id);
    const selectRowProp = {
      mode: "checkbox",
      clickToSelect: true,
      onSelect: this.onRowSelect,
      onSelectAll: this.onSelectAll
    };

    // const {
    //   showPickyDateTime,
    //   date,
    //   month,
    //   year,
    //   hour,
    //   minute,
    //   second,
    //   meridiem
    // } = this.state;
    const { open } = this.state;
    const { openC } = this.state;
    const { openG } = this.state;
    return (
      <div className="cu1">
        <div className="cu2">
          <div className="cu3">
            {" "}
            <div style={{ display: "flex" }}>
              <div>project</div>{" "}
            </div>
            <div>{this.state.members} projects</div>{" "}
          </div>
          <BootstrapTable
            search
            className="utb"
            data={this.state.dataTables}
            pagination
            options={{
              btnGroup: this.createCustomButtonGroup,
              sizePerPageDropDown: this.renderSizePerPageDropDown
            }}
            bordered={false}
            striped
            cellEdit={{
              mode: "click"
            }}
            onChange={this.onChangeUpdate}
            selectRow={selectRowProp}
          >
            <TableHeaderColumn width="80" dataField="project_id" dataSort isKey>
              {"ID"}
            </TableHeaderColumn>
            <TableHeaderColumn dataField="project_name" dataSort>
              {"Project Name"}
            </TableHeaderColumn>
            <TableHeaderColumn width="175"
              dataField="project_start"
              dataSort
              dataFormat={cell => (
                <p>{moment(cell).format("DD-MM-YYYY")}</p>
              )}
              editable={false}>
              {"Project Start"}
            </TableHeaderColumn>
            <TableHeaderColumn
              width="175"
              dataField="project_end"
              dataSort
              dataFormat={cell => (
                <p>{moment(cell).format("DD-MM-YYYY")}</p>
              )}
              editable={false}
            >
              {"Project_End"}
            </TableHeaderColumn>

            <TableHeaderColumn
              width="80"
              dataField="comname"
              dataSort
              editable={false}
            >
              {"Company"}
            </TableHeaderColumn>
            <TableHeaderColumn
              width="80"
              dataField="full_name"
              dataSort
              editable={false}
            >
              {"Usar Create"}
            </TableHeaderColumn>

          </BootstrapTable>
        </div>
        <Modal open={open} onClose={this.onCloseModal} >
          <div
            style={{
              textTransform: "uppercase",
              marginTop: "10px",
              marginBottom: "15px",
              color: "lightcyan",
              fontSize: "large"
            }}
          >
            Create Project
          </div>
          <FormGroup
            controlId="formBasicText"
            validationState={this.getValidationAddProjectnameState()}
          >
            <FormControl
              type="text"
              name="project_name"
              value={this.state.project_name}
              placeholder="Project Name"
              onChange={this.handleAddProjectname}
            />
            <FormControl.Feedback />
          </FormGroup>

          <div className="csp">
            <div className="csp1">
              <div
                style={{
                  textTransform: "uppercase",
                  marginBottom: "15px",
                  color: "lightcyan",
                  fontSize: "large"
                }}
              >
                Project Start
          </div>
              <PickyDateTime
                
                size="xs"
                mode={0}
                locale="en-us"
                // defaultDate={`${date}/${month}/${year}`}
                show={true}
                onYearPicked={res => this.onYearPicked(res)}
                onMonthPicked={res => this.onMonthPicked(res)}
                onDatePicked={res => this.onDatePicked(res)}
                onResetDate={res => this.onResetDate(res)}
                onResetDefaultDate={res => this.onResetDefaultDate(res)}
              />
            </div>
            <div className="csp1">
              <div
                style={{
                  textTransform: "uppercase",
                  marginBottom: "15px",
                  color: "lightcyan",
                  fontSize: "large"
                }}
              >
                Project End
          </div>
              <PickyDateTime
                size="xs"
                mode={0}
                locale="en-us"
                show={true}
                onYearPicked={res => this.onYearPicked2(res)}
                onMonthPicked={res => this.onMonthPicked2(res)}
                onDatePicked={res => this.onDatePicked2(res)}
                onResetDate={res => this.onResetDate2(res)}
                onResetDefaultDate={res => this.onResetDefaultDate2(res)}
              />
            </div>
          </div>

          <div>
            <input
              type="button"
              value="Accept"
              className="cspb"
              onClick={() => this._onclick()}
            />
          </div>

        </Modal>
        <Modal open={openC} onClose={this.onCloseModalC}>
          <div
            style={{
              textTransform: "uppercase",
              marginTop: "10px",
              marginBottom: "15px",
              color: "lightcyan",
              fontSize: "large",
              width: "190px"
            }}
          >
            Enter Project ID
          </div>
          <div>
            <input
              type="text"
              placeholder="Project ID"
              id="project_id"
              name="project_id"
              onChange={this._onChange}
              // maxLength="4"
              autocorrect="off"
              autocapitalize="off"
            />
          </div>
          {/* <FormGroup
            controlId="formBasicText"
          // validationState={this.getValidationAddUsernameState()}
          >
            <FormControl
              type="text"
              name="projectID"
              // value={this.state.username}
              placeholder="Project ID"
            // onChange={this.handleAddUsername}
            />
            <FormControl.Feedback />
          </FormGroup> */}
          <div>
            <input
              type="button"
              value="Accept"
              className="ubt2"
              onClick={() => this.onChart()}
            />
          </div>
        </Modal>

        <Modal open={openG} onClose={this.onCloseModalG}>
          <div
            style={{
              textTransform: "uppercase",
              marginTop: "10px",
              marginBottom: "15px",
              color: "lightcyan",
              fontSize: "large",
              width: "190px"
            }}
          >
            Project ID CHART
          </div>
          <div style={{ width: '750px', height: 300 }}>
            <ResponsiveContainer>
              <ComposedChart
                width={500}
                height={400}
                data={this.state.dataCharts}
                margin={{
                  top: 20, right: 50, bottom: 20, left: 0,
                }}

              >
                <CartesianGrid strokeDasharray="4 4" stroke="lightcyan" />
                <XAxis dataKey="Date" stroke="lightcyan" />
                <YAxis stroke="lightcyan" />
                <Tooltip />
                <Legend />
                {/* <Line type="monotone" dataKey="hour" fill="#A3CB38" stroke="lightcyan" /> */}
                <Bar type="monotone" dataKey="percent" barSize={20} fill="#0652DD" stroke="#0652DD" />
                <Area type="monotone" dataKey="hour" fill="#009432" stroke="#009432" />
              </ComposedChart>
            </ResponsiveContainer>
          </div>
        </Modal>
      </div>
    );
  }
}
