import React, { Component } from "react";
import "../css/Screen_home.css";
import backhome from "../img/backhome.png";
import profile from "../img/profile.png";
import person from "../img/person.png";
import leave from "../img/leave.png";
import request from "../img/request.png";
import Modal from "react-responsive-modal";
import { FormControl, FormGroup, ControlLabel } from "react-bootstrap";
import swal from "sweetalert";
import { post, get } from "../service/api";

export default class Screen_home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      empID: "",
      empname: "",
      emplast: "",
      userName: "",
      rol_name: "",
      position: "",
      address: "",
      tel: "",
      empEmail: "",
      pro_name: "",
      starting_date: "",
      years: "",
      months: "",
      days: "",
      comname: "",
      adminEmail: "",
      sum_maxday: "",
      use_fullday: "",
      use_haftday: "",
      waits: "",
      approve: "",
      reject: "",
      dataTables: []
    };
  }
  componentDidMount = async () => {
    let res = localStorage.getItem("empID");
    let empID = res;
    try {
      let res = await post("/profile", { empID });
      if (res.success === true) {
        this.setState({
          dataTables: res.result,
          empID: res.result[0].empID,
          empname: res.result[0].emp_name,
          emplast: res.result[0].emp_last,
          tel: res.result[0].tel,
          empEmail: res.result[0].empEmail,
          address: res.result[0].address,
          position: res.result[0].position
        });
        // this.props.set_user(res.result[0]); //เช็ตข้อมูลลง redux
        let IDcompany = res.result[0].comID;
        try {
          let ress = await post("/sum_maxday", { IDcompany });
          console.log("ress", ress);
          if (ress.success === true) {
            this.setState({
              sum_maxday: ress.result
            });
          }
        } catch (error) {
          alert(error);
        }
        try {
          let resss = await post("/use_the_leave", { empID });
          if (resss.success === true) {
            this.setState({
              use_fullday: resss.result.data4.day,
              use_haftday: resss.result.data4.time
            });
          }
        } catch (error) {
          alert(error);
        }
        try {
          let datas = await get("/request");
          if (datas.success === true) {
            this.setState({
              waits: datas.result.waits,
              approve: datas.result.approve,
              reject: datas.result.reject
            });
          }
        } catch (error) {
          alert(error);
        }
      }
    } catch (error) {
      alert(error);
    }
  };
  state = {
    open: false
  };
  onOpenModal = () => {
    this.setState({ open: true });
  };
  onCloseModal = () => {
    this.setState({ open: false });
  };
  getValidationAddFirstnameState() {
    const name = this.state.empname;
    const length = this.state.empname.length;
    if (length >= 1 && name != " ") return "success";
    else if (length >= 1 && name === " ") return "warning";
    else if (length === 0) return "error";
    return null;
  }
  getValidationAddLastnameState() {
    const name = this.state.emplast;
    const length = this.state.emplast.length;
    if (length >= 1 && name != " ") return "success";
    else if (length >= 1 && name === " ") return "warning";
    else if (length === 0) return "error";
    return null;
  }
  getValidationAddEmailState() {
    const name = this.state.empEmail;
    const length = this.state.empEmail.length;
    if (length >= 1 && name != " ") return "success";
    else if (length >= 1 && name === " ") return "warning";
    else if (length === 0) return "error";
    return null;
  }
  getValidationAddTelephoneState() {
    const name = this.state.tel;
    const length = this.state.tel.length;
    if (length >= 1 && name != " ") return "success";
    else if (length >= 1 && name === " ") return "warning";
    else if (length === 0) return "error";
    return null;
  }
  getValidationAddAddressState() {
    const name = this.state.address;
    const length = this.state.address.length;
    if (length >= 1 && name != " ") return "success";
    else if (length >= 1 && name === " ") return "warning";
    else if (length === 0) return "error";
    return null;
  }
  handleAddfirstname = e => {
    this.setState({ empname: e.target.value });
  };
  handleAddLastname = e => {
    this.setState({ emplast: e.target.value });
  };
  handleAddEmail = e => {
    this.setState({ empEmail: e.target.value });
  };
  handleAddTelephone = e => {
    this.setState({ tel: e.target.value });
  };
  handleAddAddress = e => {
    this.setState({ address: e.target.value });
  };
  onClickUpdate = async () => {
    if (
      this.state.address == "" ||
      this.state.empname == "" ||
      this.state.emplast == "" ||
      this.state.empEmail == "" ||
      this.state.tel == ""
    ) {
      swal("Warning!", "Please fill out this form", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => {});
    } else {
      let empID = this.state.empID;
      let address = this.state.address;
      let emp_name = this.state.empname;
      let emp_last = this.state.emplast;
      let email = this.state.empEmail;
      let tel = this.state.tel;
      let obj = {
        empID,
        address,
        emp_name,
        emp_last,
        email,
        tel
      };
      // alert(JSON.stringify(obj));
      try {
        let res = await post("/update_user_pagehome", obj);
        if (res.success === true) {
          swal(res.result, " ", "success", {
            buttons: false,
            timer: 1200
          }).then(() => {
            window.location.reload();
          });
        } else {
          swal(res.message, "Error! Error! Error!", "warning", {
            buttons: false,
            timer: 1200
          }).then(() => {});
        }
      } catch (error) {
        swal(error, "Error! Error! Error!", "warning", {
          buttons: false,
          timer: 1200
        }).then(() => {});
      }
    }
  };
  render() {
    const { open } = this.state;
    return (
      <div className="ch1">
        <div className="ch2">
          <img src={backhome} style={{ width: "986px" }} />
        </div>
        <div className="ch3">
          <div className="ch3-1">
            {this.state.dataTables.map(el => {
              return (
                <div id="card-profile">
                  <div id="size-profile">
                    <img src={profile} alt="Avatar" className="ch3-1-img" />
                  </div>
                  <div id="container-profile">
                    <h4>
                      <b>{el.emp_name + " " + el.emp_last}</b>
                    </h4>
                    <p>{el.position}</p>
                  </div>
                </div>
              );
            })}
          </div>
          {this.state.dataTables.map(el => {
            return (
              <div className="ch3-2">
                <a className="ch3-2-0" onClick={this.onOpenModal}>
                  <div style={{ marginRight: "5px" }}>
                    ข้อมูลส่วนตัว
                    {/* {this.props.user.emp_name} **เรียกใช้ข้อมูลจาก redux*/}
                  </div>
                  <div>
                    <img src={person} />
                  </div>
                </a>
                <div className="ch3-2-1">
                  ชื่อ-สกุล : {el.emp_name + " " + el.emp_last}
                </div>
                <div className="ch3-2-2">ตำแหน่ง : {el.position}</div>
                <div className="ch3-2-3">
                  อายุการทำงาน : {el.years} ปี {el.months} เดือน {el.days} วัน
                </div>
                <div className="ch3-2-4">เบอร์โทร : {el.tel}</div>
                <div className="ch3-2-5">อีเมลล์ : {el.empEmail}</div>
                <div className="ch3-2-6">ที่อยู่ : {el.address}</div>
              </div>
            );
          })}

          <div className="ch3-3">
            <a href="" className="ch3-3-1">
              <div
                style={{ marginRight: "5px" }}
                onClick={() =>
                  this.props.history.push("/Screen_leave_statistic")
                }
              >
                การลา
              </div>
              <div>
                <img src={leave} style={{ width: "24px" }} />
              </div>
            </a>
            <div className="ch3-3-2">
              <div>คุณมีวันลาทั้งหมด</div>
              <div>{this.state.sum_maxday} วัน</div>
            </div>
            <div className="ch3-3-3">
              <div>ลาไปแล้ว</div>
              <div>
                {this.state.use_fullday} วัน {this.state.use_haftday} ชม.
              </div>
            </div>
            <div className="ch3-3-4">
              <div>คงเหลือสิทธิ์ลา</div>
              <div> {this.state.sum_maxday - this.state.use_fullday} วัน</div>
            </div>
            <div className="ch3-3-5">
              <div style={{ paddingBottom: "10px" }}>
                <a href="/Screen_leaverequest" className="ch3-3-5-4">
                  <div>รายการคำร้องขอลาหยุด</div>
                  <div>
                    <img
                      src={request}
                      style={{ marginLeft: "5px", width: "17px" }}
                    />
                  </div>
                </a>
              </div>
              <div className="ch3-3-5-1">
                <div>wait</div>
                <div>{this.state.waits} รายการ</div>
              </div>
              <div className="ch3-3-5-2">
                <div>approved</div>
                <div>{this.state.approve} รายการ</div>
              </div>
              <div className="ch3-3-5-3">
                <div>reject</div>
                <div>{this.state.reject} รายการ</div>
              </div>
            </div>
          </div>
        </div>
        <Modal open={open} onClose={this.onCloseModal} center>
          <div
            style={{
              textTransform: "uppercase",
              marginTop: "10px",
              marginBottom: "15px",
              color: "lightcyan",
              fontSize: "large",
              width: "380px"
            }}
          >
            CHANGE PROFILE
          </div>
          <FormGroup
            controlId="formBasicText"
            validationState={this.getValidationAddFirstnameState()}
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <div
                style={{
                  color: "lightcyan",
                  marginRight: "10px",
                  width: "25%"
                }}
              >
                ชื่อ
              </div>
              <FormControl
                type="text"
                name="empname"
                value={this.state.empname}
                onChange={this.handleAddfirstname}
              />

              <FormControl.Feedback />
            </div>
          </FormGroup>

          <FormGroup
            controlId="formBasicText"
            validationState={this.getValidationAddLastnameState()}
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <div
                style={{
                  color: "lightcyan",
                  marginRight: "10px",
                  width: "25%"
                }}
              >
                นามสกุล
              </div>
              <FormControl
                type="text"
                name="emplast"
                value={this.state.emplast}
                onChange={this.handleAddLastname}
              />
              <FormControl.Feedback />
            </div>
          </FormGroup>

          <FormGroup
            controlId="formBasicText"
            validationState={this.getValidationAddEmailState()}
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <div
                style={{
                  color: "lightcyan",
                  marginRight: "10px",
                  width: "25%"
                }}
              >
                อีเมลล์
              </div>
              <FormControl
                type="text"
                name="empEmail"
                value={this.state.empEmail}
                onChange={this.handleAddEmail}
              />
              <FormControl.Feedback />
            </div>
          </FormGroup>

          <FormGroup
            controlId="formBasicText"
            validationState={this.getValidationAddTelephoneState()}
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <div
                style={{
                  color: "lightcyan",
                  marginRight: "10px",
                  width: "25%"
                }}
              >
                เบอร์โทร
              </div>
              <FormControl
                type="text"
                name="tel"
                value={this.state.tel}
                onChange={this.handleAddTelephone}
              />
              <FormControl.Feedback />
            </div>
          </FormGroup>
          <FormGroup
            controlId="formBasicText"
            validationState={this.getValidationAddAddressState()}
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <div
                style={{
                  color: "lightcyan",
                  marginRight: "10px",
                  width: "25%"
                }}
              >
                ที่อยู่
              </div>
              <FormControl
                type="textarea"
                name="address"
                value={this.state.address}
                onChange={this.handleAddAddress}
              />
              <FormControl.Feedback />
            </div>
          </FormGroup>
          <div>
            <input
              type="button"
              value="Accept"
              className="ubt2home"
              onClick={() => this.onClickUpdate()}
            />
          </div>
        </Modal>
      </div>
    );
  }
}
