import React, { Component } from "react";
import "../css/Screen_leaverequest.css";
import "../css/select.css";
import moment from "moment";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import Modal from "react-responsive-modal";
import "react-datepicker/dist/react-datepicker.css";
import searc from "../img/magnifying-glass.png";
import { post, get } from "../service/api";
import swal from "sweetalert"

export default class Screen_leaverequest extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      // startDate: new Date(),
      leaID: "",
      full_name: "",
      leaName_type: "",
      leaName_day: "",
      date_start: "",
      date_end: "",
      lea_reason: "",
      sta_name: "",
      empID: "",
      add_empID: "",
      date_created: "",
      comname: "",
      total_day: "",
      time_day: "",
      dataTables: []


    }
  }
  componentDidMount = async () => {
    try {
      let res = await get("/alldata_leave_request");
      console.log("res", res.result);

      this.setState({ dataTables: res.result });

    } catch (error) {
      alert(error);
    }
  };
  state = {
    open: false
  };
  handleChange(date) {
    this.setState({
      startDate: date
    });
  }
  ButtonApprove = (cell, Row) => {
    return (
      <div style={{ display: "flex" }}>
        <div>
          <img className="button" src={searc} style={{ marginRight: 10, cursor: "pointer" }} onClick={() => this.onOpenModal(Row)} />

        </div>
        <div>
          {Row.sta_name == "Wait" ? (
            <button className="fbt1" onClick={()=>this.onApprove2(Row)}>Approve</button>
          ) : Row.sta_name == "Approve" ? (
            <button disabled className="fbt2" >
              Approve
            </button>
          ) : (
                <button disabled className="fbt3">
                  Reject
            </button>
              )}
        </div>

      </div>
    );
  };
  renderSizePerPageDropDown = props => {
    return (
      <div className="btn-group">
        {[10, 25, 30, 40, 50, 100].map((n, idx) => {
          const isActive = n === props.currSizePerPage ? "active" : null;
          return (
            <button
              key={idx}
              type="button"
              className={`btn btn-info ${isActive}`}
              onClick={() => props.changeSizePerPage(n)}
            >
              {n}
            </button>
          );
        })}
      </div>
    );
  };
  state = {
    open: false
  };

  onOpenModal = async (Row) => {
    this.setState({
      open: true,
      leaID:Row.leaID,
      full_name: Row.full_name,
      leaName_day: Row.leaName_day,
      date_start: Row.date_start,
      date_end: Row.date_end,
      lea_reason: Row.lea_reason
    });
    // this.setState({
    //   leaID:""
    // })
    // console.log("leaID", this.state.leaID);

    // let leaID = this.state.leaID;
    // try {
    //   await post("/get_leaverequest", leaID);
    // } catch (error) {
    //   alert(error);
    // }
  };
  onCloseModal = () => {
    this.setState({ open: false });
    // window.location.reload();
  };
  handleChange(event) {
    this.setState({ value: event.target.value });
  }
  onApprove = async () => {
    
    console.log("leaID", this.state.leaID);

    let leaID = this.state.leaID;
    try {
      await post("/approve_status", {leaID});
      
      swal("Complete", "process completed", "success");
      setTimeout(() => {
        window.location.reload();
      }, 1000);
      
    } catch (error) {
      alert(error);
    }
  }
  onApprove2 = async (Row) => {
    
    let leaID = Row.leaID
    console.log("leaID",leaID);
    
    try {
      await post("/approve_status", {leaID});
      
      swal("Complete", "process completed", "success");
      setTimeout(() => {
        window.location.reload();
      }, 1000);
      
    } catch (error) {
      alert(error);
    }
  }
  onReject = async() => {
    console.log("leaID", this.state.leaID);

    let leaID = this.state.leaID;
    try {
      await post("/reject_status", {leaID});
      swal("Complete", "process completed", "success");
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    } catch (error) {
      alert(error);
    }
  }




  render() {


    const { open } = this.state;

    return (
      <div className="clr1">
        <div className="clr2">
          <div className="clr3">leave request</div>
          {/* <button onClick={()=>this.cal_date()}>  test</button> */}
          <BootstrapTable
            search
            className="ltb"
            data={this.state.dataTables}
            pagination
            bordered={false}
            striped
            options={{
              sizePerPageDropDown: this.renderSizePerPageDropDown
            }}


          >
            <TableHeaderColumn
              width={40}
              dataField="leaID"
              dataSort="leaID"
              isKey>
              {"ID"}
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="full_name"
              dataSort>
              {"Employee Name"}
            </TableHeaderColumn>
            <TableHeaderColumn
              width={130}
              dataField="comname"
              dataSort>
              {"Company Name"}
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="leaName_type"
              dataSort>
              {"Leave Type"}
            </TableHeaderColumn>
            <TableHeaderColumn
              width={80}
              dataField="leaName_day"
              dataSort>
              {"Full/Haft"}
            </TableHeaderColumn>
            <TableHeaderColumn
              width={120}
              dataField="date_start"
              dataSort
              dataFormat={cell => <p>{moment(cell).format("L")}</p>}
            >
              {"From"}
            </TableHeaderColumn>
            <TableHeaderColumn
              width={120}
              dataField="date_end"
              dataSort
              dataFormat={cell => <p>{moment(cell).format("L")}</p>}
            >
              {"To"}
            </TableHeaderColumn>
            <TableHeaderColumn
              width={120}
              // dataFormat={this._format}
              dataField="totalall"
              dataSort>
              {"Total"}
            </TableHeaderColumn>
            <TableHeaderColumn
              width={80}
              dataField="sta_name"
              dataSort
            // dataFormat={this._status}
            >
              {"Status"}
            </TableHeaderColumn>

            <TableHeaderColumn
              width={90}
              dataField="date_created"
              dataSort
              dataFormat={cell => <p>{moment(cell).format("L")}</p>}
            >
              {"Created"}
            </TableHeaderColumn>
            <TableHeaderColumn
              width={120}
              dataField="leaID"
              dataSort
              dataFormat={this.ButtonApprove}
            >
              {"Management"}
            </TableHeaderColumn>
          </BootstrapTable>
        </div>
        <Modal open={open} onClose={this.onCloseModal} >
          <div>
            <div
              style={{
                textTransform: "uppercase",
                marginTop: "10px",
                marginBottom: "15px",
                color: "lightcyan",
                fontSize: "large"
              }}
            >
              Manage Request
            </div>

            <div className="custom-select">{this.state.full_name}</div>
            <div className="custom-select">{this.state.leaName_day}</div>
            <div className="custom-dateA">
              <div className="custom-date">{moment(this.state.date_start).format('L')}</div>
              <div className="custom-date">{moment(this.state.date_end).format('L')}</div>
            </div>
            <textarea className="custom-area" rows="4" cols="50" readOnly="readOnly">{this.state.lea_reason}</textarea>

            <div className="custom-buttonA">
              <input
                type="button"
                value="APPROVE"
                className="custom-button"
                onClick={() => this.onApprove()}
              />
              <input
                type="button"
                value="REJECT"
                className="custom-button1"
                onClick={() => this.onReject()}
              />
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}
