import React, { Component } from "react";
import "../css/Screen_quest.css";
import { Col } from "react-bootstrap";

export default class Screen_quest extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataTables: [
        {
          project_id: "1",
          project_name: "EPAM",
          project_start: "2018-07-18",
          project_end: "2019-01-01",
          project_user: "Anit"
        },
        {
          project_id: "1",
          project_name: "EPAM",
          project_start: "2018-07-18",
          project_end: "2019-01-01",
          project_user: "Anit"
        },
        {
          project_id: "1",
          project_name: "EPAM",
          project_start: "2018-07-18",
          project_end: "2019-01-01",
          project_user: "Anit"
        },
        {
          project_id: "1",
          project_name: "EPAM",
          project_start: "2018-07-18",
          project_end: "2019-01-01",
          project_user: "Anit"
        }
      ]
    };
  }
  componentDidMount = () => {
    let dataTables = this.state.dataTables;
    if (this.state.dataTables.length < 6) {
      let newLen = 6 - this.state.dataTables.length;
      let emp = new Array(newLen).fill({ project_id: "" });
      this.setState({
        dataTables: [...dataTables, ...emp]
      });
    } else if (this.state.dataTables.length === 6) {
      this.setState({
        dataTables: dataTables
      });
    } else if (this.state.dataTables.length < 12) {
      let newLen = 12 - this.state.dataTables.length;
      let emp = new Array(newLen).fill({ project_id: "" });
      this.setState({
        dataTables: [...dataTables, ...emp]
      });
    }
  };
  render() {
    return (
      <div id="style-page-quest-background">
        <div id="style-page-quest-window">
          <div id="head-quest-row">
            <div id="head-quest-style">quest</div>
          </div>
          <div id="flex-wrap">
            {this.state.dataTables.map(el => {
              return (
                <Col lg={3} id="ccol">
                  <a href="#" style={{ cursor: "default" }}>
                    <div class="flip-card">
                      <div class="flip-card-inner">
                        <div class="flip-card-front">
                          <div id="name-position">John Doe</div>
                        </div>
                        <div class="flip-card-back">
                          <div>John Doe</div>
                          <div>Architect & Engineer</div>
                          <div>We love that guy</div>
                          <div id="block-input-number">
                            <input
                              type="number"
                              id="input-number-right"
                              placeholder="00"
                              min="0"
                              max="12"
                            />
                            <div
                              style={{ position: "relative", bottom: "3px" }}
                            >
                              :
                            </div>
                            <input
                              type="number"
                              id="input-number-left"
                              placeholder="00"
                              min="0"
                              max="59"
                            />
                          </div>
                          <button id="btn-confirm">Confirm</button>
                        </div>
                      </div>
                    </div>
                  </a>
                </Col>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}
