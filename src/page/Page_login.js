import React, { Component } from "react";
import img from "../img/office one logo.png";
import "../css/Page_login.css";
import { Link } from "react-router-dom";
import swal from "sweetalert";
import user from "../img/user2.png";
import password from "../img/password.png";
import { post } from "../service/api";

export default class Page_login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
  }
  componentWillUnmount = () => {
    document.removeEventListener("keypress", this.onenter);
  };
  componentDidMount = () => {
    document.addEventListener("keypress", this.onenter);
  };
  onenter = target => {
    if (target.charCode == 13) {
      this._onclick();
    }
  };
  _onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  _onclick = async () => {
    if (this.state.username == "" || this.state.password == "") {
      swal("Warning!", "Please fill out this form", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => {
        window.location.reload();
      });
    } else {
      let obj = {
        username: this.state.username,
        password: this.state.password
      };
      try {
        let res = await post("/login", obj);
        if (res.success === true) {
          localStorage.setItem("empID", res.result);
          this.props.history.push("/Screen_home");
        } else {
          swal(res.message, " ", "warning", {
            buttons: false,
            timer: 1200
          });
        }
      } catch (error) {
        alert(error);
      }
    }
  };
  render() {
    return (
      <div
        style={{
          height: "-webkit-fill-available",
          display: "flex",
          flexDirection: "column"
        }}
      >
        <div className="cl1">
          <div className="cl3">
            <img src={img} style={{ width: "100px" }} />
          </div>
          <div className="cl2">OFFICE ONE</div>
        </div>
        <div className="cl4">
          <div className="cl9">SIGN IN</div>

          <div className="cl5" style={{ marginBottom: "25px" }}>
            <span className="sp">
              <img src={user} style={{ height: "17px" }} />
            </span>
            <input
              id="sso_login_form_account"
              name="username"
              onChange={this._onChange}
              type="text"
              placeholder="Username"
              autocorrect="off"
              autocapitalize="off"
              className="cl5-1"
            />
          </div>

          <div className="cl6" style={{ marginBottom: "60px" }}>
            <span className="sp">
              <img src={password} style={{ height: "17px" }} />
            </span>
            <input
              id="sso_login_form_password"
              name="password"
              onChange={this._onChange}
              type="password"
              placeholder="Password"
              className="cl6-1"
            />
          </div>

          <div style={{ marginBottom: "25px" }}>
            <input
              id="confirm-btn"
              name="login"
              type="submit"
              value="Login"
              class="btn"
              className="cl7"
              onClick={() => this._onclick()}
            />
          </div>

          <div>
            <Link to="/Page_forgot_password" className="cl8">
              Forgot your Password?
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
