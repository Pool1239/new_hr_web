import React, { Component } from "react";
import img from "../img/office one logo.png";
import "../css/Page_forgot_password.css";
import swal from "sweetalert";
import email from "../img/email.png";
import password from "../img/password.png";
import { post } from "../service/api";

export default class Page_forgot_password extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      password2: ""
    };
  }
  componentWillUnmount = () => {
    document.removeEventListener("keypress", this.onenter);
  };
  componentDidMount = () => {
    document.addEventListener("keypress", this.onenter);
  };
  onenter = target => {
    if (target.charCode == 13) {
      this._onclick();
    }
  };
  _onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  _onclick = async () => {
    if (
      this.state.email == "" ||
      this.state.password == "" ||
      this.state.password2 == ""
    ) {
      swal("Warning!", "Please fill out this form", "warning", {
        buttons: false,
        timer: 1200
      }).then(() => {
        window.location.reload();
      });
    } else {
      if (this.state.password !== this.state.password2) {
        swal("Error!", "Wrong Password", "error", {
          buttons: false,
          timer: 1200
        });
      } else {
        let { password, email } = this.state;
        let obj = { password, email };
        this.props.history.push("/");
        try {
          let res = await post("/re_password", obj);
          if (res.success === true) {
            alert(JSON.stringify(res.result));
          } else {
            alert(JSON.stringify(res.message));
          }
        } catch (error) {
          alert(error);
        }
      }
    }
  };

  render() {
    return (
      <div
        style={{
          height: "-webkit-fill-available",
          display: "flex",
          flexDirection: "column"
        }}
      >
        <div className="cf1">
          <div className="cf3">
            <img src={img} style={{ width: "100px" }} />
          </div>
          <div className="cf2">OFFICE ONE</div>
        </div>
        <div className="cf4">
          <div className="cf9">Reset Password</div>

          <div className="cf5" style={{ marginBottom: "25px" }}>
            <span className="sp">
              <img src={email} style={{ height: "17px", marginRight: "5px" }} />
            </span>

            <input
              id="sso_login_form_account"
              name="email"
              type="text"
              placeholder="Email"
              autocorrect="off"
              autocapitalize="off"
              className="cf5-1"
              onChange={this._onChange}
            />
          </div>

          <div className="cf5" style={{ marginBottom: "10px" }}>
            <span className="sp">
              <img
                src={password}
                style={{ height: "17px", marginRight: "5px" }}
              />
            </span>

            <input
              id="sso_login_form_password"
              name="password"
              type="password"
              placeholder="Password"
              className="cf5-1"
              onChange={this._onChange}
            />
          </div>

          <div className="cf5" style={{ marginBottom: "60px" }}>
            <span className="sp">
              <img
                src={password}
                style={{ height: "17px", marginRight: "5px" }}
              />
            </span>

            <input
              id="sso_login_form_password2"
              name="password2"
              type="password"
              placeholder="Confirm Password"
              className="cf5-1"
              onChange={this._onChange}
            />
          </div>

          <div style={{ marginBottom: "25px" }}>
            <div className="cf7" onClick={() => this._onclick()}>
              confirm
            </div>
          </div>
        </div>
      </div>
    );
  }
}
