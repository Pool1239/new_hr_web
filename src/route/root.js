import React, { Component } from "react";
import { Route, BrowserRouter, Switch } from "react-router-dom";
import AppRoute from "./screen";

import login from "../page/Page_login";
import forgot_password from "../page/Page_forgot_password";
import navbar from "../component/navbar";

export default () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={login} />
      <Route path="/Page_forgot_password" component={forgot_password} />
      <Route path="/navbar" component={navbar} />
      {/* <Route path='/flexible' component={flexible} /> */}
      {/* <Route path='/checkworking' component={checkworking} /> */}
      {/* <Route path='/test' component={test} /> */}
      <AppRoute />
    </Switch>
  </BrowserRouter>
);
