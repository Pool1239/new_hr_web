import React, { Component } from "react";
import { Route } from "react-router-dom";
import App from "../App";
import home from "../screen/Screen_home";
import leaverequest from "../screen/Screen_leaverequest";
import flexible from "../screen/Screen_flexible";
import user from "../screen/Screen_user";
import company from "../screen/Screen_company";
import myleaverequest from "../screen/Screen_myleaverequest";
import myflexible from "../screen/Screen_myflexiblerequest";
import reportleave from "../screen/Screen_report_leave";
import reportflexible from "../screen/Screen_report_flexible";
import statistic from "../screen/Screen_leave_statistic";
import quest from "../screen/Screen_quest";
import project from "../screen/Screen_project";
import work from "../screen/Screen_work";

export default () => (
  <App>
    <Route path="/Screen_home" component={home} />
    <Route path="/Screen_leaverequest" component={leaverequest} />
    <Route path="/Screen_flexible" component={flexible} />
    <Route path="/Screen_user" component={user} />
    <Route path="/Screen_company" component={company} />
    <Route path="/Screen_myleaverequest" component={myleaverequest} />
    <Route path="/Screen_report_leave" component={reportleave} />
    <Route path="/Screen_report_flexible" component={reportflexible} />
    <Route path="/Screen_myflexiblerequest" component={myflexible} />
    <Route path="/Screen_leave_statistic" component={statistic} />
    <Route path="/Screen_quest" component={quest} />
    <Route path="/Screen_project" component={project} />
    <Route path="/Screen_work" component={work} />
    {/* <Route path='/AllPeople' component={AllPeople} /> */}
    {/* <Route path='/job' component={job} /> */}
    {/* <Route path='/job2' component={job2} /> */}
  </App>
);
